#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function
import os
import shutil
import codecs
import re
from list_function import make_array, pattern_matching, filtering, match_awalan, match_musik, match_aksara, match_bahasa, match_suku

array_all_heritage_provinsi = make_array()
array_all_heritage_kab_kota = make_array()
array_all_heritage_all = make_array()

path = "C:/temp_skripsi/skripsi/pywikibot/cultural_heritage/"
path_kab_kota = "C:/temp_skripsi/skripsi/pywikibot/cultural_heritage_kab_kota/"

src = "C:/temp_skripsi/skripsi/pywikibot/"
dest = "C:/temp_skripsi/skripsi/pywikibot/collection/"

directory_names = ["aksara", "alat_musik", "bahasa", "benteng", "candi", "cerita_rakyat", "istana", "kerajaan", "kolam", "komunitas_adat", "kuil", "lagu", "makam", "makanan", "masjid", "motif", "musik", "naskah", "ornamen", "pakaian", "patung", "permainan", "petirtaan", "prasasti", "pura", "ritual", "rumah", "seni_pertunjukkan", "senjata", "situs", "suku", "tari", "tradisi_lisan"]

#mengambil entitas dari provinsi
kategori = 0
for directory_name in directory_names:
	directory = path + directory_name
	entity_arr = []
	for filename in os.listdir(directory):
		f_path = directory + "/" + filename
		if os.path.getsize(f_path) > 0:
			#mendapatkan nama provinsi
			provinsi = filename.split('_')[-1].replace(".txt", "").strip()
			#membuka isi file
			with open(f_path, 'r') as f:
				entity_property_arr = f.read().strip().split('\n')
				for entity_property in entity_property_arr:
					#mengambil entity
					entity = entity_property.split("|", 1)[0].strip()
					entity_arr.append(entity + " | " + provinsi + " | " + "None")
	entity_pattern = pattern_matching(entity_arr)
	entity_clean_list = filtering(entity_pattern)
	array_all_heritage_provinsi[kategori] = entity_clean_list
	kategori = kategori + 1

#mengambil entitas dari kabupaten/kota
kategori = 0
for directory_name in directory_names:
	directory = path_kab_kota + directory_name
	entity_arr = []
	for filename in os.listdir(directory):
		f_path = directory + "/" + filename
		if os.path.getsize(f_path) > 0:
			#mendapatkan nama kabupaten/kota
			kabupaten_kota = filename.split('_')[-1].replace(".txt", "").strip()
			#mendapatkan nama provinsi
			provinsi = filename.split('_')[-2].strip()
			#membuka isi file
			with open(f_path, 'r') as f:
				entity_property_arr = f.read().strip().split('\n')
				for entity_property in entity_property_arr:
					#mengambil entity
					entity = entity_property.split("|", 1)[0].strip()
					entity_arr.append(entity + " | " + provinsi + " | " + kabupaten_kota)
	entity_pattern = pattern_matching(entity_arr)
	entity_clean_list = filtering(entity_pattern)
	array_all_heritage_kab_kota[kategori] = entity_clean_list
	kategori = kategori + 1

#menggabungkan entitas dari provinsi dan kabupaten/kota
kategori = 0
for kategori in range(len(directory_names)):
	array_all_heritage_all[kategori] = array_all_heritage_provinsi[kategori] + array_all_heritage_kab_kota[kategori]
	kategori = kategori + 1

#menuliskan ke berkas
f_kategori = codecs.open('collection_temp.csv', 'w')
f_all_data = open("all_collection.txt", "w")
kategori = 0
for directory_name in directory_names:
	#menghilangkan duplikasi
	arr_unique_entity = []
	for elem in array_all_heritage_all[kategori]:
		parts = elem.split(" | ")
		entity = parts[0]
		provinsi = parts[1]
		kabupaten_kota = parts[2]
		line = entity.strip().lower() + " | " + provinsi.strip().lower() + " | " + kabupaten_kota.strip().lower()
		#cek duplikat
		if line not in arr_unique_entity:
			f_kategori.write(directory_name + "," + entity + "," + provinsi + "," + kabupaten_kota + "\n")
			arr_unique_entity.append(line)
			print (entity, file=f_all_data)
	kategori = kategori + 1
f_kategori.close()

#melakukan pengubahan kategori
f_final = codecs.open('collection.csv', 'w')
f_final.write("kategori,entity,provinsi,kabupaten/kota\n")
with open('collection_temp.csv', 'r') as coll:
	for linecoll in coll:
		#mendapatkan nama kategori, entitas, provinsi, dan kab/kota
		partscoll = linecoll.split(",")
		kategori = directory_names.index(partscoll[0].strip())
		entity = partscoll[1].strip()
		provinsi = partscoll[2].strip()
		kabupaten_kota = partscoll[3].strip()

		#filtering
		kategori_awalan = match_awalan(entity)
		if kategori != kategori_awalan and kategori_awalan != -1:
			kategori = kategori_awalan
			
		#formatting pada kategori aksara, bahasa, dan suku
		if kategori == 0 and not match_aksara(entity):
			entity = "Aksara " + entity
		if kategori == 2 and not match_bahasa(entity):
			if re.match(r'^dialek\s(.)+', entity, re.I):
				if re.search(r'^jawa tengah', provinsi, re.I) or re.search(r'^jawa timur', provinsi, re.I):
					entity = "Bahasa Jawa " + entity
				elif re.search(r'^jawa barat', provinsi, re.I):
					entity = "Bahasa Sunda " + entity
			else:
				entity = "Bahasa " + entity
		if kategori == 30 and not match_suku(entity):
			entity = "Suku " + entity
		if kategori == 30 and re.match(r'^etnis\s(.)+', entity, re.I):
			entity = re.sub('^etnis', 'Suku', entity, flags=re.IGNORECASE)
		f_final.write(str(kategori) + "," + entity + "," + provinsi + "," + kabupaten_kota + "\n")
f_final.close()