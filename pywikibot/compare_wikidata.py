#!/usr/bin/python
# -- coding: utf-8 --

from __future__ import print_function
from SPARQLWrapper import SPARQLWrapper, JSON
import pandas as pd

#difference antara wikidata dengan hasil ekstraksi
#memasukkan yang hasil ekstraksi

f_found = open("wikidatafound.txt", "w")
f_not_found = open("wikidatanotfound.txt", "w")

arr_aksara = ["Aksara Suwawa", "Aksara Dewanagari", "Aksara Sasak", "Aksara Lampung", "Aksara Sunda Baku", "Aksara Batak Toba"]

for aksara in arr_aksara:
	query = "SELECT ?item WHERE { ?item rdfs:label \'" + aksara + "\'@id . }"
	sparql = SPARQLWrapper("https://query.wikidata.org/sparql")
	sparql.setQuery(query)

	sparql.setReturnFormat(JSON)
	results = sparql.query().convert()
	results_df = pd.io.json.json_normalize(results['results']['bindings'])
	try:
		print (results_df[['item.value']].head())
		print (aksara, file=f_found)
	except:
		print (aksara, file=f_not_found)