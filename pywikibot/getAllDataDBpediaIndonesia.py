#!/usr/bin/python
# -- coding: utf-8 --

from __future__ import print_function
from SPARQLWrapper import SPARQLWrapper, XML
import xml.etree.ElementTree as ET
from list_function import make_array 

array_all_heritage_dbpedia_id = make_array()
sparql_dbpedia_id = SPARQLWrapper("http://id.dbpedia.org/sparql")
with open("query_dbpedia_ind.csv", 'r') as allquery:
	for linequery in allquery:
		partscoll = linequery.split(",", 1)
		kategori = int(partscoll[0].strip())
		query = partscoll[1].strip()
		sparql_dbpedia_id.setQuery(query)
		sparql_dbpedia_id.setReturnFormat(XML)
		results = sparql_dbpedia_id.query().convert()
		root = ET.fromstring(results.toxml().encode('utf-8'))
		for literal in root.iter("{http://www.w3.org/2005/sparql-results#}literal"):
			array_all_heritage_dbpedia_id[kategori].append(literal.text.encode('utf-8'))