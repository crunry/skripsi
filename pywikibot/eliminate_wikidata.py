from __future__ import print_function
import requests

f_not_in_wikidata = open("step2_notfoundinwikidataenng.txt", "w")
f_in_wikidata = open("step2_foundinwikidataenng.txt", "w")

print ("PROGRAM STARTS")
with open('filter_step_2.txt', 'r') as diff:
	for line in diff:
		#bahasa yang digunakan bahasa inggris
		entitas = line.strip()
		api_en = "https://www.wikidata.org/w/api.php?action=wbsearchentities&search=" + entitas + "&language=en&format=json"
		api_id = "https://www.wikidata.org/w/api.php?action=wbsearchentities&search=" + entitas + "&language=id&format=json"
		response_en = requests.get(api_en)
		response_id = requests.get(api_id)
		data_en = response_en.json()
		data_id = response_id.json()
		if len(data_en['search']) == 0 and len(data_id['search']) == 0:
			print (entitas + " not found in wikidata")
			print (entitas, file=f_not_in_wikidata)
		else:
			print (entitas + " found in wikidata")
			print (entitas, file=f_in_wikidata)
print ("WIKIDATA DONE")