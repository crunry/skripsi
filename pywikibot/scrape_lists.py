#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from wikitables import import_tables
from bs4 import BeautifulSoup
import codecs
import re
import sys
import nltk.data
import requests
from list_function import getAllSectionTitle, make_empty, make_array, match_awalan, getKategoriIndex, fillProvinceHeritageMap, printToFileAllHeritage, getProperty, printStatisticTable

reload(sys)  
sys.setdefaultencoding('utf8')

provinsi_heritage = {} 
array_all_heritage_list = make_array()
array_h2_section_not_included = ["galeri", "pranala luar", "referensi", "catatan kaki", "rujukan", "bacaan lanjutan", "lihat pula", "lihat juga", "daftar isi", "pustaka", "keterangan", "bacaan lanjut"]
tables = import_tables('Daftar provinsi di Indonesia', 'id')
fname_section ='section_list.xls'
f_section = codecs.open(fname_section, 'w')
#digunakan untuk menyimpan nama-nama provinsi
array_nama_provinsi = []
for row in tables[0].rows:
	nama_provinsi = '{Provinsi}'.format(**row)
	array_nama_provinsi.append(nama_provinsi)
	provinsi_heritage[nama_provinsi] = {}
	wiki = "https://id.wikipedia.org/wiki/" + nama_provinsi
	header = {'User-Agent': 'Mozilla/5.0'}
	page = requests.get(wiki, headers=header)
	soup = BeautifulSoup(page.content, "lxml")
	
	#digunakan untuk memberikan index baris pada halaman HTML
	map_index_baris = {}
	num = 1
	arr_line = str(soup).split("\n")
	for line in arr_line:
		map_index_baris[line] = num
		num = num + 1

	lists = soup.findAll("li")
	#filter list daftar
	filtered_lists = [li for li in lists if not 'tocsection' in ''.join(li.get('class', []))]
	#filter list citation
	filtered_lists_2 = [li for li in filtered_lists if not 'cite_note' in ''.join(li.get('id', []))]
	#filter list footer
	filtered_lists_3 = [li for li in filtered_lists_2 if not 'footer' in ''.join(li.get('id', []))]
	#filter list navigasi box
	filtered_lists_4 = [li for li in filtered_lists_3 if not 'nv' in ''.join(li.get('class', []))]
	#filter list menu navigasi
	filtered_lists_5 = [li for li in filtered_lists_4 if not 'pt-' in ''.join(li.get('id', []))]
	filtered_lists_6 = [li for li in filtered_lists_5 if not 't-' in ''.join(li.get('id', []))]
	filtered_lists_7 = [li for li in filtered_lists_6 if not 'n-' in ''.join(li.get('id', []))]
	filtered_lists_8 = [li for li in filtered_lists_7 if not 'collapsible' in ''.join(li.get('class', []))]
	filtered_lists_9 = [li for li in filtered_lists_8 if not 'coll-' in ''.join(li.get('id', []))]
	filtered_lists_10 = [li for li in filtered_lists_9 if not 'interlanguage-' in ''.join(li.get('class', []))]
	filtered_lists_11 = [li for li in filtered_lists_10 if not 'ca-' in ''.join(li.get('id', []))]
	filtered_lists_12 = [li for li in filtered_lists_11 if not 'wb-' in ''.join(li.get('class', []))]
	#menghilangkan yang judul h2 section-nya lihat pula, pranala luar, dan referensi
	filtered_lists_13 = []
	#menghilangkan yang judul h2 section-nya mengandung kata pada array section not included
	for li in filtered_lists_12:
		h2 = li.find_previous("h2")
		if h2 is not None:
			if not h2.getText().replace('[sunting | sunting sumber]','').strip().lower() in array_h2_section_not_included:
				filtered_lists_13.append(li)
	for li in filtered_lists_13:
		section_label = getAllSectionTitle(li, map_index_baris)
		li_text = li.getText().encode('utf-8')
		f_section.write(nama_provinsi + "\t" + li_text.strip() + "\t" + section_label.encode('utf-8') + "\n")
		#menggunakan kata awalan untuk meng-kategorikan
		kategori_awalan = match_awalan(li_text)
		if kategori_awalan != -1:
			prop = getProperty("list", li, map_index_baris)
			entity_property = li_text + " | " + prop
			if not entity_property in array_all_heritage_list[kategori_awalan]:
				array_all_heritage_list[kategori_awalan].append(entity_property) 
		#mengambil judul section sebelumnya
		h2 = li.find_previous("h2")
		h3 = li.find_previous("h3")
		h4 = li.find_previous("h4")
		h5 = li.find_previous("h5")
		h6 = li.find_previous("h6")
		h6_text = ""
		h5_text = ""
		h4_text = ""
		h3_text = ""
		h2_text = ""
		index_baris_h2 = -1
		index_baris_h3 = -1
		index_baris_h4 = -1
		index_baris_h5 = -1
		index_baris_h6 = -1
		for key, value in map_index_baris.iteritems():
			if h6 is not None:
				if str(h6) in str(key):
					index_baris_h6 = value
			if h5 is not None:
				if str(h5) in str(key):
					index_baris_h5 = value
			if h4 is not None:
				if str(h4) in str(key):
					index_baris_h4 = value
			if h3 is not None:
				if str(h3) in str(key):
					index_baris_h3 = value
			if h2 is not None:
				if str(h2) in str(key):
					index_baris_h2 = value
		try:
			if index_baris_h6 > index_baris_h5 > index_baris_h4 > index_baris_h3 > index_baris_h2:
				h6_text = h6.getText().replace('[sunting | sunting sumber]','').strip()
				#tokenisasi
				token_h6 = nltk.word_tokenize(h6_text.lower())
				#membuat bigram
				bigram_h6 = list(nltk.bigrams(token_h6))
				for (a,b) in bigram_h6:
					token_h6.append(a + " " + b)
				h5_text = h5.getText().replace('[sunting | sunting sumber]','').strip()
				#tokenisasi
				token_h5 = nltk.word_tokenize(h5_text.lower())
				#membuat bigram
				bigram_h5 = list(nltk.bigrams(token_h5))
				for (a,b) in bigram_h5:
					token_h5.append(a + " " + b)
				h4_text = h4.getText().replace('[sunting | sunting sumber]','').strip()
				#tokenisasi
				token_h4 = nltk.word_tokenize(h4_text.lower())
				#membuat bigram
				bigram_h4 = list(nltk.bigrams(token_h4))
				for (a,b) in bigram_h4:
					token_h4.append(a + " " + b)
				h3_text = h3.getText().replace('[sunting | sunting sumber]','').strip()
				token_h3 = nltk.word_tokenize(h3_text.lower())
				#membuat bigram
				bigram_h3 = list(nltk.bigrams(token_h3))
				#membuat bigram
				for (a,b) in bigram_h3:
					token_h3.append(a + " " + b)
				h2_text = h2.getText().replace('[sunting | sunting sumber]','').strip()
				token_h2 = nltk.word_tokenize(h2_text.lower())
				#membuat bigram
				bigram_h2 = list(nltk.bigrams(token_h2))
				#membuat bigram
				for (a,b) in bigram_h2:
					token_h2.append(a + " " + b)
				entity_property = li_text + " | " + h6_text + " - " + h5_text + " - " + h4_text + " - " + h3_text + " - " + h2_text
				#menggunakan h4 sebagai judul section
				kategori = getKategoriIndex(token_h6)
				#jika tidak ditemukan di h6, gunakan h5
				if kategori == -1:
					kategori = getKategoriIndex(token_h5)
					#jika tidak ditemukan di h5, gunakan h4	
					if kategori == -1:
						kategori = getKategoriIndex(token_h4)
						#jika tidak ditemukan di h4, gunakan h3
						if kategori == -1:
							kategori = getKategoriIndex(token_h3)
							#jika tidak ditemukan di h3, gunakan h2
							if kategori == -1:
								kategori = getKategoriIndex(token_h2)
								#jika ditemukan di h2
								if kategori != -1:
									if not entity_property in array_all_heritage_list[kategori]:
										array_all_heritage_list[kategori].append(entity_property)
							#jika ditemukan di h3
							else:
								if not entity_property in array_all_heritage_list[kategori]:
									array_all_heritage_list[kategori].append(entity_property)
						#jika ditemukan di h4
						else:
							if not entity_property in array_all_heritage_list[kategori]:
								array_all_heritage_list[kategori].append(entity_property)
					#jika ditemukan di h5
					else:
						if not entity_property in array_all_heritage_list[kategori]:
							array_all_heritage_list[kategori].append(entity_property)
				#jika ditemukan di h6
				else:
					if not entity_property in array_all_heritage_list[kategori]:
						array_all_heritage_list[kategori].append(entity_property)
			elif index_baris_h5 > index_baris_h4 > index_baris_h3 > index_baris_h2:
				h5_text = h5.getText().replace('[sunting | sunting sumber]','').strip()
				#tokenisasi
				token_h5 = nltk.word_tokenize(h5_text.lower())
				#membuat bigram
				bigram_h5 = list(nltk.bigrams(token_h5))
				for (a,b) in bigram_h5:
					token_h5.append(a + " " + b)
				h4_text = h4.getText().replace('[sunting | sunting sumber]','').strip()
				#tokenisasi
				token_h4 = nltk.word_tokenize(h4_text.lower())
				#membuat bigram
				bigram_h4 = list(nltk.bigrams(token_h4))
				for (a,b) in bigram_h4:
					token_h4.append(a + " " + b)
				h3_text = h3.getText().replace('[sunting | sunting sumber]','').strip()
				token_h3 = nltk.word_tokenize(h3_text.lower())
				#membuat bigram
				bigram_h3 = list(nltk.bigrams(token_h3))
				#membuat bigram
				for (a,b) in bigram_h3:
					token_h3.append(a + " " + b)
				h2_text = h2.getText().replace('[sunting | sunting sumber]','').strip()
				token_h2 = nltk.word_tokenize(h2_text.lower())
				#membuat bigram
				bigram_h2 = list(nltk.bigrams(token_h2))
				#membuat bigram
				for (a,b) in bigram_h2:
					token_h2.append(a + " " + b)
				entity_property = li_text + " | " + h5_text + " - " + h4_text + " - " + h3_text + " - " + h2_text
				kategori = getKategoriIndex(token_h5)
				#jika tidak ditemukan di h5, gunakan h4	
				if kategori == -1:
					kategori = getKategoriIndex(token_h4)
					#jika tidak ditemukan di h4, gunakan h3
					if kategori == -1:
						kategori = getKategoriIndex(token_h3)
						#jika tidak ditemukan di h3, gunakan h2
						if kategori == -1:
							kategori = getKategoriIndex(token_h2)
							#jika ditemukan di h2
							if kategori != -1:
								if not entity_property in array_all_heritage_list[kategori]:
									array_all_heritage_list[kategori].append(entity_property)
						#jika ditemukan di h3
						else:
							if not entity_property in array_all_heritage_list[kategori]:
								array_all_heritage_list[kategori].append(entity_property)
					#jika ditemukan di h4
					else:
						if not entity_property in array_all_heritage_list[kategori]:
							array_all_heritage_list[kategori].append(entity_property)
				#jika ditemukan di h5
				else:
					if not entity_property in array_all_heritage_list[kategori]:
						array_all_heritage_list[kategori].append(entity_property)
			elif index_baris_h4 > index_baris_h3 > index_baris_h2:
				h4_text = h4.getText().replace('[sunting | sunting sumber]','').strip()
				#tokenisasi
				token_h4 = nltk.word_tokenize(h4_text.lower())
				#membuat bigram
				bigram_h4 = list(nltk.bigrams(token_h4))
				for (a,b) in bigram_h4:
					token_h4.append(a + " " + b)
				h3_text = h3.getText().replace('[sunting | sunting sumber]','').strip()
				token_h3 = nltk.word_tokenize(h3_text.lower())
				#membuat bigram
				bigram_h3 = list(nltk.bigrams(token_h3))
				#membuat bigram
				for (a,b) in bigram_h3:
					token_h3.append(a + " " + b)
				h2_text = h2.getText().replace('[sunting | sunting sumber]','').strip()
				#tokenisasi
				token_h2 = nltk.word_tokenize(h2_text.lower())
				#membuat bigram
				bigram_h2 = list(nltk.bigrams(token_h2))
				for (a,b) in bigram_h2:
					token_h2.append(a + " " + b)
				entity_property = li_text + " | " + h4_text + " - " + h3_text + " - " + h2_text
				#menggunakan h4 sebagai judul section
				kategori = getKategoriIndex(token_h4)
				#jika tidak ditemukan di h4, gunakan h3
				if kategori == -1:
					kategori = getKategoriIndex(token_h3)
					#jika tidak ditemukan di h3, gunakan h2
					if kategori == -1:
						kategori = getKategoriIndex(token_h2)
						#jika ditemukan di h2
						if kategori != -1:
							if not entity_property in array_all_heritage_list[kategori]:
								array_all_heritage_list[kategori].append(entity_property)
					#jika ditemukan di h3
					else:
						if not entity_property in array_all_heritage_list[kategori]:
							array_all_heritage_list[kategori].append(entity_property)
				#jika ditemukan di h4
				else:
					if not entity_property in array_all_heritage_list[kategori]:
						array_all_heritage_list[kategori].append(entity_property)
				
			elif index_baris_h3 > index_baris_h2:
					h3_text = h3.getText().replace('[sunting | sunting sumber]','').strip()
					#tokenisasi
					token_h3 = nltk.word_tokenize(h3_text.lower())
					#membuat bigram
					bigram_h3 = list(nltk.bigrams(token_h3))
					for (a,b) in bigram_h3:
						token_h3.append(a + " " + b)
					h2_text = h2.getText().replace('[sunting | sunting sumber]','').strip()
					token_h2 = nltk.word_tokenize(h2_text.lower())
					#membuat bigram
					bigram_h2 = list(nltk.bigrams(token_h2))
					#membuat bigram
					for (a,b) in bigram_h2:
						token_h2.append(a + " " + b)
					entity_property = li_text + " | " + h3_text + " - " + h2_text
					#menggunakan h3 sebagai judul section
					kategori = getKategoriIndex(token_h3)
					#jika tidak ditemukan di h3, gunakan h2
					if kategori == -1:
						kategori = getKategoriIndex(token_h2)
						#jika ditemukan di h2
						if kategori != -1:
							if not entity_property in array_all_heritage_list[kategori]:
								array_all_heritage_list[kategori].append(entity_property)
					#jika ditemukan di h3
					else:
						if not entity_property in array_all_heritage_list[kategori]:
							array_all_heritage_list[kategori].append(entity_property)
			elif index_baris_h2 != -1:
				h2_text = h2.getText().replace('[sunting | sunting sumber]','').strip()
				token_h2 = nltk.word_tokenize(h2_text.lower())
				#membuat bigram
				bigram_h2 = list(nltk.bigrams(token_h2))
				#membuat bigram
				for (a,b) in bigram_h2:
					token_h2.append(a + " " + b)
				entity_property = li_text + " | " + h2_text
				#menggunakan h2 sebagai judul section
				kategori = getKategoriIndex(token_h2)
				#jika tidak ditemukan di h3, gunakan h2
				if kategori != -1:
					if not entity_property in array_all_heritage_list[kategori]:
						array_all_heritage_list[kategori].append(entity_property)
		except:
			print ("judul section berantakan")
	fillProvinceHeritageMap(provinsi_heritage, nama_provinsi, array_all_heritage_list)
	printToFileAllHeritage("lists", provinsi_heritage, nama_provinsi)
	make_empty(array_all_heritage_list)
	print ("Lists in " + nama_provinsi + " done... ")
printStatisticTable("lists", array_nama_provinsi, provinsi_heritage)
f_section.close()