<?php

require_once __DIR__.'/../vendor/autoload.php';

// create sentence detector
$sentenceDetectorFactory = new \Sastrawi\SentenceDetector\SentenceDetectorFactory();
$sentenceDetector = $sentenceDetectorFactory->createSentenceDetector();

//ambil berkas yang berisikan semua kalimat
$file_konten = fopen("C:/temp_skripsi/skripsi/pywikibot/examples/all_content.txt", "r") or die("Unable to open file!");
#berkas yang berisikan kalimat-kalimat
$file_kalimat = fopen("C:/temp_skripsi/skripsi/pywikibot/examples/all_sentence.txt", "w") or die("Unable to open file!");
while(!feof($file_konten)) {
	$text = fgets($file_konten);
	// detect sentence
	$sentences = $sentenceDetector->detect($text);
	foreach ($sentences as $i => $sentence) {
		fwrite($file_kalimat, "$sentence\n");
	}
}
fclose($file_konten);
fclose($file_kalimat);