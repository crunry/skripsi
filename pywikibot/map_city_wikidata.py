from __future__ import print_function
import requests
import re

f_mapping_city = open("city_mapping.txt", "w")

print ("PROGRAM STARTS")
with open('regency.txt', 'r') as cities:
	for line in cities:
		entitas = line.strip()
		api_id = "https://www.wikidata.org/w/api.php?action=wbsearchentities&search=" + entitas + "&language=id&format=json"
		response_id = requests.get(api_id)
		data_id = response_id.json()
		#mendapatkan id Q
		search_result = data_id["search"]
		print (entitas + "\t" + search_result[0]["title"], file=f_mapping_city)
		print ("ID of " + entitas + " got successfully")
print ("MAPPING IS DONE")