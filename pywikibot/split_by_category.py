from __future__ import print_function
import codecs
import shutil
from list_function import make_array

#melakukan pemisahan entitas budaya berdasarkan kategori

directory_names = ["aksara", "alat_musik", "bahasa", "benteng", "candi", "cerita_rakyat", "istana", "kerajaan", "kolam", "komunitas_adat", "kuil", "lagu", "makam", "makanan", "masjid", "motif", "musik", "naskah", "ornamen", "pakaian", "patung", "permainan", "petirtaan", "prasasti", "pura", "ritual", "rumah", "seni_pertunjukkan", "senjata", "situs", "suku", "tari", "tradisi_lisan"]

array_all_heritage = make_array()

src = "C:/skripsi_hadi/skripsi/pywikibot/"
dst = "C:/skripsi_hadi/skripsi/pywikibot/splitting/"

#sebelum menggunakan program ini, jangan lupa hilangkan duplikasi terlebih dahulu
#yang disimpan hanyalah nama entitas saja
with open('collection_unique.csv', 'r') as coll:
	for linecoll in coll:
		#mendapatkan nama kategori, entitas
		partscoll = linecoll.split(",")
		#kategori berupa angka
		kategori = int(partscoll[0].strip())
		entity = partscoll[1].strip()
		array_all_heritage[kategori].append(entity)


kategori = 0
for arr_kategori in array_all_heritage:
	filename = directory_names[kategori] + '.csv'
	f = codecs.open(filename, 'w')
	for entity in arr_kategori:
		f.write(entity + "\n")
	f.close()
	shutil.move(src + filename, dst + filename)
	kategori = kategori + 1

