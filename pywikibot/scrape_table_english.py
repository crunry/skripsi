#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function
from wikitables import import_tables
from bs4 import BeautifulSoup
import codecs
import re
import nltk.data
import requests
from list_function import contain_punctuation_or_contain_digit, make_empty, make_array, match_awalan, fillProvinceHeritageMap, printToFileAllHeritage, getKategoriIndex, getProperty, printStatisticTable

#map yang digunakan untuk menyimpan entitas budaya
# provinsi_heritage = {}
# array_all_heritage = make_array()
tables = import_tables('Provinces of Indonesia', 'en')
jumlah_provinsi = len(tables[0].rows)
#digunakan dalam pembuatan kamus budaya
fname_header ='header_en.xls'
f_head = codecs.open(fname_header, 'w')

#digunakan dalam pembuatan kamus awalan
fname_sel ='sel_en.xls'
f_sel = codecs.open(fname_sel, 'w')
#digunakan untuk menyimpan nama-nama provinsi
array_nama_provinsi = []
for row in tables[0].rows:
	nama_provinsi = '{Provinsi}'.format(**row)
	# array_nama_provinsi.append(nama_provinsi)
	# provinsi_heritage[nama_provinsi] = {}
	wiki = "https://en.wikipedia.org/wiki/" + nama_provinsi
	header = {'User-Agent': 'Mozilla/5.0'}
	page = requests.get(wiki, headers=header)
	soup = BeautifulSoup(page.content, "lxml")

	#digunakan untuk memberikan index baris pada halaman HTML
	# map_index_baris = {}
	# num = 1
	# arr_line = str(soup).split("\n")
	# for line in arr_line:
	# 	map_index_baris[line] = num
	# 	num = num + 1

	tables = soup.findAll("table", {"class" : "wikitable"})
	tn = 0
	for table in tables:
		table = tables[tn]
		# parsed_table_data = []
		rows = table.findAll('tr')
		nrows = len(rows)
		# kolom_kategori_array = []
		header_prop = ""
		for row in rows:
			children = row.findChildren(recursive=False)
			# row_text = []
			# digunakan untuk mendapatkan index kolom
			kolom = 0
			for child in children:
				#untuk mengambil keseluruhan isi tabel
				clean_text = child.text
				#untuk menghilangkan link referensi atau citation
				clean_text = clean_text.split('&#91;')[0]
				#untuk menghilangkan ikon sort pada header row
				clean_text = clean_text.split('&#160;')[-1]
				clean_text = clean_text.strip()
				#jika ditemukan header yang mengandung kata dalam kamus
				# for kolom_kategori in kolom_kategori_array:
				# 	kolom_budaya = kolom_kategori[0]
				# 	kategori_budaya = kolom_kategori[1]
				# 	if kolom == kolom_budaya:
				# 		prop = getProperty("table", table, map_index_baris)
						# entity_property = clean_text + " | " + prop + " | " + header_prop
						# if not entity_property in array_all_heritage[kategori_budaya]:
						# 	array_all_heritage[kategori_budaya].append(entity_property)

				if child.name == "th":
					header_prop = clean_text.lower()
					f_head.write(nama_provinsi + "\t" + header_prop + "\n")
					#jika header berhubungan dengan kata yang mendeskripsikan kategori warisan budaya
					# token_clean_text = nltk.word_tokenize(clean_text.lower())
					# bigram = list(nltk.bigrams(token_clean_text))
					# #membuat bigram
					# for (a,b) in bigram:
					# 	token_clean_text.append(a + " " + b)
					# kategori = getKategoriIndex(token_clean_text)
					# if kategori != -1:
					# 	kolom_kategori_array.append((kolom, kategori, clean_text))

				#jika teks yang diproses cocok dengan awalan pada kamus
				# kategori_awalan = match_awalan(clean_text)
				# if kategori_awalan != -1:
					# prop = getProperty("table", table, map_index_baris)
					# entity_property = clean_text + " | " + prop + " | " + header_prop
					# if not entity_property in array_all_heritage[kategori_awalan]:
					# 	array_all_heritage[kategori_awalan].append(entity_property)
				# row_text.append(clean_text)
				f_sel.write(nama_provinsi + "\t" + clean_text.lower() + "\t" + header_prop + "\n")
				kolom = kolom + 1
			# parsed_table_data.append(row_text)

		# #untuk mencetak semua tabel ke format csv
		# fname='output_{}_t{}.csv'.format(nama_provinsi,tn)
		# f = codecs.open(fname, 'w')
		# for i in range(nrows):
		# 	rowStr = ','.join(parsed_table_data[i])
		# 	rowStr = rowStr.replace('\n','')
		# 	rowStr = rowStr.encode('utf-8')
		# 	f.write(rowStr  +'\n')      
		# f.close()
		# tn = tn + 1    
			
	# fillProvinceHeritageMap(provinsi_heritage, nama_provinsi, array_all_heritage)
	# printToFileAllHeritage("table", provinsi_heritage, nama_provinsi)
	# make_empty(array_all_heritage)   
	print ("Tables in " + nama_provinsi + " done...")
# printStatisticTable("table", array_nama_provinsi, provinsi_heritage)
#untuk menutup berkas yang menyimpan header dan sel tabel
f_sel.close()
f_head.close()