#!/usr/bin/python
# -- coding: utf-8 --

from __future__ import print_function
from SPARQLWrapper import SPARQLWrapper, XML
import xml.etree.ElementTree as ET
import goslate
import sys
from list_function import make_array 

reload(sys)  
sys.setdefaultencoding('utf8')

gs = goslate.Goslate()

sparql_dbpedia = SPARQLWrapper("http://dbpedia.org/sparql")
with open("query_dbpedia.txt", 'r') as allquery:
	for linequery in allquery:
		partscoll = linequery.split(",", 1)
		kategori = int(partscoll[0].strip())
		query = partscoll[1].strip()
		sparql_dbpedia.setQuery(query)
		sparql_dbpedia.setReturnFormat(XML)
		results = sparql_dbpedia.query().convert()
		root = ET.fromstring(results.toxml().encode('utf-8'))
		for literal in root.iter("{http://www.w3.org/2005/sparql-results#}literal"):
			# print (gs.translate(literal.text.encode('utf-8'), 'en', 'id'))
			print ("running...")

			

