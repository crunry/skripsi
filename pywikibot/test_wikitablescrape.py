import os
import shutil

import wikitablescrape

try:
    shutil.rmtree('output')
except FileNotFoundError:
    pass

wikitablescrape.scrape(
    url="https://id.wikipedia.org/wiki/Daftar_kabupaten_dan_kota_di_Indonesia",
    output_name="kota_kabupaten"
)

os.makedirs('output')
shutil.move('./kota_kabupaten', './output')
