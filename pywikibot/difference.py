#!/usr/bin/python
# -- coding: utf-8 --

from __future__ import print_function
from SPARQLWrapper import SPARQLWrapper, JSON
import xml.etree.ElementTree as ET
import pandas as pd

arr_not_found_wikidata_id = []
arr_not_found_wikidata_en = []
arr_not_found_dbpedia_internasional = []
arr_not_found_dbpedia_indonesia = []
f_not_found_wikidata_id = open("notfoundinwikidataid.txt", "w")
f_not_found_wikidata_en = open("notfoundinwikidataen.txt", "w")
f_not_found_dbpedia_internasional = open("notfoundindbpediainter.txt", "w")
f_not_found_dbpedia_indonesia = open("notfoundindbpediaindo.txt", "w")

print ("PROGRAM STARTS")
with open('difference.txt', 'r') as diff:
	for line in diff:
		entitas = line.strip()
		#label berbahasa Indonesia
		query = "SELECT ?item WHERE { ?item rdfs:label \'" + entitas + "\'@id . }"
		sparql = SPARQLWrapper("https://query.wikidata.org/sparql")
		sparql.setQuery(query)
		sparql.setReturnFormat(JSON)
		try:
			results = sparql.query().convert()
			results_df = pd.io.json.json_normalize(results['results']['bindings'])
			print (results_df[['item.value']].head())
		except:
			print ("NOT FOUND")
			arr_not_found_wikidata_id.append(entitas)
			print (entitas, file=f_not_found_wikidata_id)

print ("WIKIDATA INDONESIA DONE")

for entitas in arr_not_found_wikidata_id:
	#label berbahasa Inggris
	query = "SELECT ?item WHERE { ?item rdfs:label \'" + entitas + "\'@en . }"
	sparql = SPARQLWrapper("https://query.wikidata.org/sparql")
	sparql.setQuery(query)
	sparql.setReturnFormat(JSON)
	try:
		results = sparql.query().convert()
		results_df = pd.io.json.json_normalize(results['results']['bindings'])
		print (results_df[['item.value']].head())
	except:
		print ("NOT FOUND")
		arr_not_found_wikidata_en.append(entitas)
		print (entitas, file=f_not_found_wikidata_en)

print ("WIKIDATA ENGLISH DONE")

for entitas in arr_not_found_wikidata_en:
	#query dbpedia internasional 
	query = "SELECT ?item WHERE { ?item rdfs:label \'" + entitas + "\'@en . }"
	sparql = SPARQLWrapper("https://dbpedia.org/sparql")
	sparql.setQuery(query)
	sparql.setReturnFormat(JSON)
	try:
		results = sparql.query().convert()
		results_df = pd.io.json.json_normalize(results['results']['bindings'])
		print (results_df[['item.value']].head())
	except:
		print ("NOT FOUND")
		arr_not_found_dbpedia_internasional.append(entitas)
		print (entitas, file=f_not_found_dbpedia_internasional)

print ("DBPEDIA INTERNASIONAL DONE")

with open('notfoundindbpediainter.txt', 'r') as inter:
	for line in inter:
		entitas = line.strip()
		#query dbpedia Indonesia
		query = "SELECT ?item WHERE { ?item rdfs:label \'" + entitas + "\'@id . }"
		sparql = SPARQLWrapper("http://dbpedia.cs.ui.ac.id/sparql")
		sparql.setQuery(query)
		sparql.setReturnFormat(JSON)
		results = sparql.query().convert()
		results_df = pd.io.json.json_normalize(results['results']['bindings'])
		try:
			print (results_df[['item.value']].head())
		except:
			print ("NOT FOUND")
			arr_not_found_dbpedia_indonesia.append(entitas)
			print (entitas, file=f_not_found_dbpedia_indonesia)

print ("DBPEDIA INDONESIA DONE")