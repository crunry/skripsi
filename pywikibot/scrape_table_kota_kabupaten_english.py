#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function
from bs4 import BeautifulSoup
import urllib2
import codecs
import re
import requests
import sys
import nltk.data
from list_function import contain_punctuation_or_contain_digit, make_empty, make_array, match_awalan, fillProvinceHeritageMapKabupatenKota, printToFileAllHeritageKabupatenKota, getKategoriIndex, getProperty, printStatisticTableKabupatenKota

reload(sys)  
sys.setdefaultencoding('utf8')

#map yang digunakan untuk menyimpan entitas budaya
# kabupaten_kota_heritage = {}
#map yang menyimpan nama provinsi beserta kabupaten dan kotanya
# map_kabupaten_kota = {}
header = {'User-Agent': 'Mozilla/5.0'}
wiki_kabupaten_kota = "https://en.wikipedia.org/wiki/<<diisi>>"
req_kabupaten_kota = urllib2.Request(wiki_kabupaten_kota, headers=header)
page_kabupaten_kota = urllib2.urlopen(req_kabupaten_kota)
soup_kabupaten_kota = BeautifulSoup(page_kabupaten_kota, "lxml")
tables_kabupaten_kota = soup_kabupaten_kota.findAll("table", {"class" : "wikitable"})
#digunakan dalam pembuatan kamus budaya
fname_header ='header_kabupaten_kota_en.xls'
f_head = codecs.open(fname_header, 'w')
#digunakan dalam pembuatan kamus awalan
fname_sel ='sel_kabupaten_kota_en.xls'
f_sel = codecs.open(fname_sel, 'w')
#ambil entitas kabupaten dan kota dari tabel 1-34
t = 1
while (t <= 34):
	#mendapatkan nama provinsi
	h3 = tables_kabupaten_kota[t].find_previous("h3")
	nama_provinsi = ""
	if h3 is not None:
		#DIGANTI
		nama_provinsi = h3.getText().replace('[sunting | sunting sumber]','').decode('utf-8')
	# kabupaten_kota_heritage[nama_provinsi] = {}
	# map_kabupaten_kota[nama_provinsi] = []
	# array_all_heritage  = make_array()
	kolom_kabupaten_kota = -1
	counter_kolom = 0
	for row_header in tables_kabupaten_kota[t].findAll("th"):
		clean_text = row_header.text
		#untuk menghilangkan link referensi atau citation
		clean_text = clean_text.split('&#91;')[0]
		#untuk menghilangkan ikon sort pada header row
		clean_text = clean_text.split('&#160;')[-1]
		clean_text = clean_text.strip()
		if clean_text == "Kabupaten/Kota":
			kolom_kabupaten_kota = counter_kolom
			break
		counter_kolom = counter_kolom + 1
	if nama_provinsi == "Jakarta":
		kolom_kabupaten_kota = 1
	rows_kabupaten_kota = tables_kabupaten_kota[t].findAll('tr')
	for row_kabupaten_kota in rows_kabupaten_kota:
		children_kabupaten_kota = row_kabupaten_kota.findChildren(recursive=False)
		counter_kolom = 0
		for child_kabupaten_kota in children_kabupaten_kota:
			#ambil pada kolom yang diinginkan saja
			if counter_kolom == kolom_kabupaten_kota:
				nama_kabupaten_kota = child_kabupaten_kota.text
				#untuk menghilangkan link referensi atau citation
				nama_kabupaten_kota = nama_kabupaten_kota.split('&#91;')[0]
				#untuk menghilangkan ikon sort pada header row
				nama_kabupaten_kota = nama_kabupaten_kota.split('&#160;')[-1]
				nama_kabupaten_kota = re.sub('\n\[\d+\]', '', nama_kabupaten_kota)
				nama_kabupaten_kota = nama_kabupaten_kota.strip()
				if nama_kabupaten_kota != "Kabupaten/Kota" and nama_kabupaten_kota != "Nama" and nama_kabupaten_kota != "Perubahan" and nama_kabupaten_kota != "Desa/\nKelurahan":
					map_kabupaten_kota[nama_provinsi].append(nama_kabupaten_kota)
					kabupaten_kota_heritage[nama_provinsi][nama_kabupaten_kota] = {}
					wiki = "https://en.wikipedia.org/wiki/" + nama_kabupaten_kota.replace('\s','_')
					page = requests.get(wiki, headers=header)
					soup = BeautifulSoup(page.content, "lxml")

					#digunakan untuk memberikan index baris pada halaman HTML
					# map_index_baris = {}
					# num = 1
					# arr_line = str(soup).split("\n")
					# for line in arr_line:
					# 	map_index_baris[line] = num
					# 	num = num + 1

					tables = soup.findAll("table", {"class" : "wikitable"})
					tn = 0
					for table in tables:
						table = tables[tn]
						# parsed_table_data = []
						rows = table.findAll('tr')
						nrows = len(rows)
						# kolom_kategori_array = []
						header_prop = ""
						for row in rows:
							children = row.findChildren(recursive=False)
							# row_text = []
							#digunakan untuk mendapatkan index kolom
							kolom = 0
							for child in children:
								#untuk mengambil keseluruhan isi tabel
								clean_text = child.text
								#untuk menghilangkan link referensi atau citation
								clean_text = clean_text.split('&#91;')[0]
								#untuk menghilangkan ikon sort pada header row
								clean_text = clean_text.split('&#160;')[-1]
								clean_text = clean_text.strip()
								#jika ditemukan header yang mengandung kata dalam kamus
								# for kolom_kategori in kolom_kategori_array:
								# 	kolom_budaya = kolom_kategori[0]
								# 	kategori_budaya = kolom_kategori[1]
								# 	header_prop = kolom_kategori[2]
								# 	if kolom == kolom_budaya:
								# 		prop = getProperty("table", table, map_index_baris)
								# 		entity_property = clean_text + " | " + prop + " | " + header_prop
								# 		if not entity_property in array_all_heritage[kategori_budaya]:
								# 			array_all_heritage[kategori_budaya].append(entity_property)

								if child.name == "th":
									header_prop = clean_text.lower()
									f_head.write(nama_provinsi + "\t" + nama_kabupaten_kota + "\t" + header_prop + "\n")
									#jika header berhubungan dengan kata yang mendeskripsikan kategori warisan budaya
									# token_clean_text = nltk.word_tokenize(clean_text.lower())
									# #membuat bigram
									# bigram = list(nltk.bigrams(token_clean_text))
									# for (a,b) in bigram:
									# 	token_clean_text.append(a + " " + b)
									# kategori = getKategoriIndex(token_clean_text)
									# if kategori != -1:
									# 	kolom_kategori_array.append((kolom, kategori, clean_text))

								#jika teks yang diproses cocok dengan awalan pada kamus
								# kategori_awalan = match_awalan(clean_text)
								# if kategori_awalan != -1:
								# 	prop = getProperty("table", table, map_index_baris)
								# 	entity_property = clean_text + " | " + prop + " | " + header_prop
								# 	if not entity_property in array_all_heritage[kategori_awalan]:
								# 		array_all_heritage[kategori_awalan].append(entity_property)
								# row_text.append(clean_text)
								f_sel.write(nama_provinsi + "\t" + nama_kabupaten_kota + "\t" + clean_text.lower() + "\t" + header_prop + "\n")
								kolom = kolom + 1
							# parsed_table_data.append(row_text)

						# #untuk mencetak semua tabel ke format csv
						# fname='output_{}_{}_t{}.csv'.format(nama_provinsi, nama_kabupaten_kota,tn)
						# f = codecs.open(fname, 'w')
						# for i in range(nrows):
						# 	rowStr = ','.join(parsed_table_data[i])
						# 	rowStr = rowStr.replace('\n','')
						# 	rowStr = rowStr.encode('utf-8')
						# 	f.write(rowStr  +'\n')      
						# f.close()
						# tn = tn + 1
     
					# fillProvinceHeritageMapKabupatenKota(kabupaten_kota_heritage, nama_provinsi, nama_kabupaten_kota, array_all_heritage)
					# printToFileAllHeritageKabupatenKota("table", kabupaten_kota_heritage, nama_provinsi, nama_kabupaten_kota)
					# make_empty(array_all_heritage)  
					print ("Tables in " + nama_provinsi + ": "  + nama_kabupaten_kota + " done...")
			counter_kolom = counter_kolom + 1
	t = t + 1
# printStatisticTableKabupatenKota("table", map_kabupaten_kota, kabupaten_kota_heritage)
#untuk menutup berkas yang menyimpan header tabel
f_head.close()
f_sel.close()