#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function
from __future__ import unicode_literals
from wikitables import import_tables
import wikipedia as wp
from bs4 import BeautifulSoup
import codecs
import re
import sys
import nltk.data
import shutil
import os
import subprocess
import requests
from polyglot.text import Text
from polyglot.detect import Detector
from list_function import make_empty, make_array, match_awalan, getKategoriIndex, fillProvinceHeritageMap, printToFileAllHeritage, getPOSTag, printStatisticTable

reload(sys)  
sys.setdefaultencoding('utf8')

src = "C:/temp_skripsi/skripsi/pywikibot/"
dest = "C:/temp_skripsi/skripsi/pywikibot/examples/"

arr_kategori_awalan = [0, 2, 3, 4, 6, 7, 8, 10, 12, 14, 16, 17, 20, 22, 23, 24, 26, 29, 30, 31]
provinsi_heritage = {} 
array_all_heritage_sentences = make_array()
tables = import_tables('Daftar provinsi di Indonesia', 'id')

#digunakan untuk menyimpan relasi yang terekstrak
fname_relasi ='relasi_free_text.xls'
f_relasi = codecs.open(fname_relasi, 'w')
f_relasi.write("kategori\tpola\tkalimat\thipernim\thiponim\tprovinsi\n")

#digunakan untuk menyimpan entitas yang berawalan
fname_awalan ='awalan_free_text.xls'
f_awalan = codecs.open(fname_awalan, 'w')
f_awalan.write("kategori\tentitas\tkalimat\tprovinsi\n")

#digunakan untuk mengetahui apa saja yang disaring/tidak disaring NER
f_filterNER = open("filterNER.txt", "w")
f_allNER = open("allNER.txt", "w")

#digunakan untuk menyimpan semua kata benda di bagian kalimat yang mengandung hipernim
fname_hipernim ='noun_hipernim.xls'
f_hipernim = codecs.open(fname_hipernim, 'w')

#digunakan untuk menyimpan semua kata benda di bagian kalimat yang mengandung hiponim
fname_hiponim ='noun_hiponim.xls'
f_hiponim = codecs.open(fname_hiponim, 'w')

#digunakan untuk menyimpan nama-nama provinsi
array_nama_provinsi = []
for row in tables[0].rows:
	nama_provinsi = '{Provinsi}'.format(**row)
	array_nama_provinsi.append(nama_provinsi)
	provinsi_heritage[nama_provinsi] = {}
	wp.set_lang('id')
	halaman = wp.page(nama_provinsi)
	#untuk menghilangkan section
	filter_konten = re.sub(r"=(.)+=", "", halaman.content)
	#untuk menghilangkan new line berlebihan
	filter_konten_2 = re.sub(r'(\n){2,}','\n', filter_konten)
	#menuliskan ke dalam berkas .txt
	f_kontenname = 'all_content.txt'
	f_konten = open(f_kontenname, "w")
	print (filter_konten_2.encode('utf-8'), file=f_konten)
	f_konten.close()
	shutil.move(src + f_kontenname, dest + f_kontenname)
	#membagi konten menjadi kalimat-kalimat
	subprocess.call("php C:/temp_skripsi/skripsi/pywikibot/examples/complex_sentence.php")
	f_sentences = open("C:/temp_skripsi/skripsi/pywikibot/examples/all_sentence.txt", "r")
	lists = f_sentences.readlines()
	for kalimat in lists:
		#hanya mengolah kalimat yang berbahasa indonesia
		#dideteksi menggunakan language detector
		try:
			#menghilangkan new line pada kalimat
			kalimat = re.sub(r'\n','', kalimat)
			detector = Detector(kalimat)
			if detector.language.code == "id":
				#mengambil kalimat yang bersih
				#diawali dengan huruf kapital
				#dan diakhiri dengan tanda titik, seru, atau tanya
				match_kalimat = re.match(r'^[A-Z].*[.!?]$', kalimat)
				if match_kalimat:
					text = Text(kalimat, hint_language_code='id')
					#menyimpan NER
					map_entity_ner = {}
					for sent in text.sentences:
						for entity in sent.entities:
							string_entity = " ".join(entity)
							map_entity_ner[string_entity] = entity.tag
							print (string_entity, file=f_allNER)
					#menambahkan tag start di awal kalimat dan menambahkan tag end di akhir kalimat
					kalimat_tag = "<start> " + kalimat + " <end>"
					#menggunakan pattern matching untuk mengambil kalimat yang sesuai dengan pola
					match_pola_1 = re.match(r'.+\s(seperti)\s.+\s(dan)\s.+', kalimat_tag)
					if match_pola_1:
						words = []
						tags = []
						index = 0
						index_seperti = -1
						index_dan = -1
						for word, tag in text.pos_tags:
							words.append(word)
							tags.append(tag)
							if word == "seperti":
								index_seperti = index
							elif word == "dan":
								index_dan = index
							index = index + 1

						parts = re.split(' seperti ', kalimat_tag, 1)

						#mendapatkan hipernim
						left = parts[0].replace("<start> ", "")
						tokens_left = nltk.word_tokenize(left.lower())
						bigram_left = list(nltk.bigrams(tokens_left)) 
						for (a,b) in bigram_left:
							tokens_left.append(a + " " + b)
						for temp in tokens_left:
							f_hipernim.write(nama_provinsi + "\t" + temp + "\n")
						kategori = getKategoriIndex(tokens_left)

						#mengambil hiponim
						words_hyponym = words[index_seperti+1:index_dan]
						tags_hyponym = tags[index_seperti+1:index_dan]
						#mendapatkan PROPN atau NOUN berurutan
						start_index = -1
						end_index = -1
						i = 0
						while i < len(words_hyponym):
							if tags_hyponym[i] == "PROPN":
								start_index = i
								j = i + 1
								while j < len(tags_hyponym) and tags_hyponym[j] == "PROPN":
									j = j + 1
								end_index = j - 1
								if start_index == end_index:
									hyponym = words_hyponym[start_index]
									if hyponym in map_entity_ner:
										print (hyponym, file=f_filterNER)
									if kategori != -1 and hyponym not in map_entity_ner:
										f_relasi.write(str(kategori) + "\t" + str(1) + "\t" + kalimat + "\t" + left + "\t" + hyponym + "\t" + nama_provinsi + "\n")
										if not hyponym in array_all_heritage_sentences[kategori]:
											array_all_heritage_sentences[kategori].append(hyponym)
								else:
									start = start_index
									end = end_index
									hyponym = ""
									while start <= end:
										hyponym = hyponym + " " + words_hyponym[start]
										start = start + 1
									hyponym = hyponym.strip()
									if hyponym in map_entity_ner:
										print (hyponym, file=f_filterNER)
									if kategori not in arr_kategori_awalan and kategori != -1 and hyponym not in map_entity_ner:
										f_relasi.write(str(kategori) + "\t" + str(1) + "\t" + kalimat + "\t" + left + "\t" + hyponym + "\t" + nama_provinsi + "\n")
										if not hyponym in array_all_heritage_sentences[kategori]:
											array_all_heritage_sentences[kategori].append(hyponym)
								i = j + 1
							else:
								i = i + 1

					match_pola_2 = re.match(r'.+\s(termasuk)\s.+', kalimat_tag)
					if match_pola_2:
						words = []
						tags = []
						index = 0
						index_termasuk = -1
						for word, tag in text.pos_tags:
							words.append(word)
							tags.append(tag)
							if word == "termasuk":
								index_termasuk = index
							index = index + 1

						parts = re.split(' termasuk ', kalimat_tag, 1)

						#mendapatkan hypernym
						left = parts[0].replace("<start> ", "")
						tokens_left = nltk.word_tokenize(left.lower())
						bigram_left = list(nltk.bigrams(tokens_left)) 
						for (a,b) in bigram_left:
							tokens_left.append(a + " " + b)
						for temp in tokens_left:
							f_hipernim.write(nama_provinsi + "\t" + temp + "\n")
						kategori = getKategoriIndex(tokens_left)

						#mengambil hiponim
						words_hyponym = words[index_termasuk+1:]
						tags_hyponym = tags[index_termasuk+1:]
						#mendapatkan PROPN atau NOUN berurutan
						start_index = -1
						end_index = -1
						i = 0
						while i < len(tags_hyponym):
							if tags_hyponym[i] == "PROPN":
								start_index = i
								j = i + 1
								while j < len(tags_hyponym) and tags_hyponym[j] == "PROPN":
									j = j + 1
								end_index = j - 1
								if start_index == end_index:
									hyponym = words_hyponym[start_index]
									if hyponym in map_entity_ner:
										print (hyponym, file=f_filterNER)
									if kategori != -1 and hyponym not in map_entity_ner:
										f_relasi.write(str(kategori) + "\t" + str(2) + "\t" + kalimat + "\t" + left + "\t" + hyponym + "\t" + nama_provinsi + "\n")
										if not hyponym in array_all_heritage_sentences[kategori]:
											array_all_heritage_sentences[kategori].append(hyponym)
								else:
									start = start_index
									end = end_index
									hyponym = ""
									while start <= end:
										hyponym = hyponym + " " + words_hyponym[start]
										start = start + 1
									hyponym = hyponym.strip()
									if hyponym in map_entity_ner:
										print (hyponym, file=f_filterNER)
									if kategori not in arr_kategori_awalan and kategori != -1 and hyponym not in map_entity_ner:
										f_relasi.write(str(kategori) + "\t" + str(2) + "\t" + kalimat + "\t" + left + "\t" + hyponym + "\t" + nama_provinsi + "\n")
										if not hyponym in array_all_heritage_sentences[kategori]:
											array_all_heritage_sentences[kategori].append(hyponym)
								i = j + 1
							else:
								i = i + 1
						
					match_pola_3 = re.match(r'.+\s(adalah)\s.+\s(yang)\s.+', kalimat_tag)
					if match_pola_3:
						words = []
						tags = []
						index = 0
						index_adalah = -1
						index_yang = -1
						for word, tag in text.pos_tags:
							words.append(word)
							tags.append(tag)

							if word == "adalah":
								index_adalah = index
							elif word == "yang":
								index_yang = index
							index = index + 1

						parts = re.split(' adalah ', kalimat_tag, 1)

						#mendapatkan hipernim
						hypernym = re.sub('\syang.*', '', parts[1]).strip()
						tokens_hypernym = nltk.word_tokenize(hypernym.lower())
						bigram_hypernym = list(nltk.bigrams(tokens_hypernym)) 
						for (a,b) in bigram_hypernym:
							tokens_hypernym.append(a + " " + b)
						for temp in tokens_hypernym:
							f_hipernim.write(nama_provinsi + "\t" + temp + "\n")
						kategori = getKategoriIndex(tokens_hypernym)

						#mengambil hiponim
						words_hyponym = words[:index_adalah]
						tags_hyponym = tags[:index_adalah]
						#mendapatkan PROPN atau NOUN berurutan
						start_index = -1
						end_index = -1
						i = 0
						while i < len(tags_hyponym):
							if tags_hyponym[i] == "PROPN":
								start_index = i
								j = i + 1
								while j < len(tags_hyponym) and tags_hyponym[j] == "PROPN":
									j = j + 1
								end_index = j - 1
								if start_index == end_index:
									hyponym = words_hyponym[start_index]
									if hyponym in map_entity_ner:
										print (hyponym, file=f_filterNER)
									if kategori != -1 and hyponym not in map_entity_ner:
										f_relasi.write(str(kategori) + "\t" + str(3) + "\t" + kalimat + "\t" + hypernym + "\t" + hyponym + "\t" + nama_provinsi + "\n")
										if not hyponym in array_all_heritage_sentences[kategori]:
											array_all_heritage_sentences[kategori].append(hyponym)
								else:
									start = start_index
									end = end_index
									hyponym = ""
									while start <= end:
										hyponym = hyponym + " " + words_hyponym[start]
										start = start + 1
									hyponym = hyponym.strip()
									if hyponym in map_entity_ner:
										print (hyponym, file=f_filterNER)
									if kategori not in arr_kategori_awalan and kategori != -1 and hyponym not in map_entity_ner:
										f_relasi.write(str(kategori) + "\t" + str(3) + "\t" + kalimat + "\t" + hypernym + "\t" + hyponym + "\t" + nama_provinsi + "\n")
										if not hyponym in array_all_heritage_sentences[kategori]:
											array_all_heritage_sentences[kategori].append(hyponym)
								i = j + 1
							else:
								i = i + 1

					match_pola_4 = re.match(r'.+\s(adalah)\s(sebuah)\s.+', kalimat_tag)
					if match_pola_4:
						words = []
						tags = []
						index = 0
						index_adalah = -1
						for word, tag in text.pos_tags:
							words.append(word)
							tags.append(tag)

							if word == "adalah":
								index_adalah = index
							index = index + 1

						parts = re.split(' adalah sebuah ', kalimat_tag, 1)

						#mendapatkan hipernim
						right = parts[1].replace(" <end>", "")
						tokens_right = nltk.word_tokenize(right.lower())
						bigram_right = list(nltk.bigrams(tokens_right)) 
						for (a,b) in bigram_right:
							tokens_right.append(a + " " + b)
						for temp in tokens_right:
							f_hipernim.write(nama_provinsi + "\t" + temp + "\n")
						kategori = getKategoriIndex(tokens_right)

						#mengambil hiponim
						words_hyponym = words[:index_adalah]
						tags_hyponym = tags[:index_adalah]
						#mendapatkan PROPN atau NOUN berurutan
						start_index = -1
						end_index = -1
						i = 0
						while i < len(tags_hyponym):
							if tags_hyponym[i] == "PROPN":
								start_index = i
								j = i + 1
								while j < len(tags_hyponym) and tags_hyponym[j] == "PROPN":
									j = j + 1
								end_index = j - 1
								if start_index == end_index:
									hyponym = words_hyponym[start_index]
									if hyponym in map_entity_ner:
										print (hyponym, file=f_filterNER)
									if kategori != -1 and hyponym not in map_entity_ner:
										f_relasi.write(str(kategori) + "\t" + str(4) + "\t" + kalimat + "\t" + right + "\t" + hyponym + "\t" + nama_provinsi + "\n")
										if not hyponym in array_all_heritage_sentences[kategori]:
											array_all_heritage_sentences[kategori].append(hyponym)
								else:
									start = start_index
									end = end_index
									hyponym = ""
									while start <= end:
										hyponym = hyponym + " " + words_hyponym[start]
										start = start + 1
									hyponym = hyponym.strip()
									if hyponym in map_entity_ner:
										print (hyponym, file=f_filterNER)
									if kategori not in arr_kategori_awalan and kategori != -1 and hyponym not in map_entity_ner:
										f_relasi.write(str(kategori) + "\t" + str(4) + "\t" + kalimat + "\t" + right + "\t" + hyponym + "\t" + nama_provinsi + "\n")
										if not hyponym in array_all_heritage_sentences[kategori]:
											array_all_heritage_sentences[kategori].append(hyponym)
								i = j + 1
							else:
								i = i + 1

					match_pola_5 = re.match(r'.+\s(menjadi)\s.+', kalimat_tag)
					if match_pola_5:
						words = []
						tags = []
						index = 0
						index_menjadi = -1
						for word, tag in text.pos_tags:
							words.append(word)
							tags.append(tag)
							if word == "menjadi":
								index_menjadi = index
							index = index + 1

						parts = re.split(' menjadi ', kalimat_tag, 1)

						#mendapatkan hipernim
						right = parts[1].replace(" <end>", "")
						tokens_right = nltk.word_tokenize(right.lower())
						bigram_right = list(nltk.bigrams(tokens_right)) 
						for (a,b) in bigram_right:
							tokens_right.append(a + " " + b)
						for temp in tokens_right:
							f_hipernim.write(nama_provinsi + "\t" + temp + "\n")
						kategori = getKategoriIndex(tokens_right)

						#mengambil hiponim
						words_hyponym = words[:index_menjadi]
						tags_hyponym = tags[:index_menjadi]
						#mendapatkan PROPN atau NOUN berurutan
						start_index = -1
						end_index = -1
						i = 0
						while i < len(tags_hyponym):
							if tags_hyponym[i] == "PROPN":
								start_index = i
								j = i + 1
								while j < len(tags_hyponym) and tags_hyponym[j] == "PROPN":
									j = j + 1
								end_index = j - 1
								if start_index == end_index:
									hyponym = words_hyponym[start_index]
									if hyponym in map_entity_ner:
										print (hyponym, file=f_filterNER)
									if kategori != -1 and hyponym not in map_entity_ner:
										f_relasi.write(str(kategori) + "\t" + str(5) + "\t" + kalimat + "\t" + right + "\t" + hyponym + "\t" + nama_provinsi + "\n")
										if not hyponym in array_all_heritage_sentences[kategori]:
											array_all_heritage_sentences[kategori].append(hyponym)
								else:
									start = start_index
									end = end_index
									hyponym = ""
									while start <= end:
										hyponym = hyponym + " " + words_hyponym[start]
										start = start + 1
									hyponym = hyponym.strip()
									if hyponym in map_entity_ner:
										print (hyponym, file=f_filterNER)
									
									if kategori not in arr_kategori_awalan and kategori != -1 and hyponym not in map_entity_ner:
										f_relasi.write(str(kategori) + "\t" + str(5) + "\t" + kalimat + "\t" + right + "\t" + hyponym + "\t" + nama_provinsi + "\n")
										if not hyponym in array_all_heritage_sentences[kategori]:
											array_all_heritage_sentences[kategori].append(hyponym)
								i = j + 1
							else:
								i = i + 1

					match_pola_6 = re.match(r'.+\s(merupakan)\s.+', kalimat_tag)
					if match_pola_6:
						words = []
						tags = []
						index = 0
						index_merupakan = -1
						for word, tag in text.pos_tags:
							words.append(word)
							tags.append(tag)
							if word == "merupakan":
								index_merupakan = index
							index = index + 1

						parts = re.split(' merupakan ', kalimat_tag, 1)

						#mendapatkan hipernim
						right = parts[1].replace(" <end>", "")
						tokens_right = nltk.word_tokenize(right.lower())
						bigram_right = list(nltk.bigrams(tokens_right)) 
						for (a,b) in bigram_right:
							tokens_right.append(a + " " + b)
						for temp in tokens_right:
							f_hipernim.write(nama_provinsi + "\t" + temp + "\n")
						kategori = getKategoriIndex(tokens_right)

						#mengambil hiponim
						words_hyponym = words[:index_merupakan]
						tags_hyponym = tags[:index_merupakan]
						#mendapatkan PROPN atau NOUN berurutan
						start_index = -1
						end_index = -1
						i = 0
						while i < len(tags_hyponym):
							if tags_hyponym[i] == "PROPN":
								start_index = i
								j = i + 1
								while j < len(tags_hyponym) and tags_hyponym[j] == "PROPN":
									j = j + 1
								end_index = j - 1
								if start_index == end_index:
									hyponym = words_hyponym[start_index]
									if hyponym in map_entity_ner:
										print (hyponym, file=f_filterNER)
									if kategori != -1 and hyponym not in map_entity_ner:
										f_relasi.write(str(kategori) + "\t" + str(6) + "\t" + kalimat + "\t" + right + "\t" + hyponym + "\t" + nama_provinsi + "\n")
										if not hyponym in array_all_heritage_sentences[kategori]:
											array_all_heritage_sentences[kategori].append(hyponym)
								else:
									start = start_index
									end = end_index
									hyponym = ""
									while start <= end:
										hyponym = hyponym + " " + words_hyponym[start]
										start = start + 1
									hyponym = hyponym.strip()
									if hyponym in map_entity_ner:
										print (hyponym, file=f_filterNER)
									
									if kategori not in arr_kategori_awalan and kategori != -1 and hyponym not in map_entity_ner:
										f_relasi.write(str(kategori) + "\t" + str(6) + "\t" + kalimat + "\t" + right + "\t" + hyponym + "\t" + nama_provinsi + "\n")
										if not hyponym in array_all_heritage_sentences[kategori]:
											array_all_heritage_sentences[kategori].append(hyponym)
								i = j + 1
							else:
								i = i + 1

					match_pola_7 = re.match(r'.+\s(merupakan)\s.+\s(yang)\s.+', kalimat_tag)
					if match_pola_7:
						words = []
						tags = []
						index = 0
						index_merupakan = -1
						for word, tag in text.pos_tags:
							words.append(word)
							tags.append(tag)
							if word == "merupakan":
								index_merupakan = index
							index = index + 1

						parts = re.split(' merupakan ', kalimat_tag, 1)

						#mendapatkan hipernim
						hypernym = re.sub('\syang.*', '', parts[1]).strip()
						tokens_hypernym = nltk.word_tokenize(hypernym.lower())
						bigram_hypernym = list(nltk.bigrams(tokens_hypernym)) 
						for (a,b) in bigram_hypernym:
							tokens_hypernym.append(a + " " + b)
						for temp in tokens_hypernym:
							f_hipernim.write(nama_provinsi + "\t" + temp + "\n")
						kategori = getKategoriIndex(tokens_hypernym)

						#mengambil hiponim
						words_hyponym = words[:index_merupakan]
						tags_hyponym = tags[:index_merupakan]
						#mendapatkan PROPN atau NOUN berurutan
						start_index = -1
						end_index = -1
						i = 0
						while i < len(tags_hyponym):
							if tags_hyponym[i] == "PROPN":
								start_index = i
								j = i + 1
								while j < len(tags_hyponym) and tags_hyponym[j] == "PROPN":
									j = j + 1
								end_index = j - 1
								if start_index == end_index:
									hyponym = words_hyponym[start_index]
									if hyponym in map_entity_ner:
										print (hyponym, file=f_filterNER)
									if kategori != -1 and hyponym not in map_entity_ner:
										f_relasi.write(str(kategori) + "\t" + str(7) + "\t" + kalimat + "\t" + hypernym + "\t" + hyponym + "\t" + nama_provinsi + "\n")
										if not hyponym in array_all_heritage_sentences[kategori]:
											array_all_heritage_sentences[kategori].append(hyponym)
								else:
									start = start_index
									end = end_index
									hyponym = ""
									while start <= end:
										hyponym = hyponym + " " + words_hyponym[start]
										start = start + 1
									hyponym = hyponym.strip()
									if hyponym in map_entity_ner:
										print (hyponym, file=f_filterNER)
									
									if kategori not in arr_kategori_awalan and kategori != -1 and hyponym not in map_entity_ner:
										f_relasi.write(str(kategori) + "\t" + str(7) + "\t" + kalimat + "\t" + hypernym + "\t" + hyponym + "\t" + nama_provinsi + "\n")
										if not hyponym in array_all_heritage_sentences[kategori]:
											array_all_heritage_sentences[kategori].append(hyponym)
								i = j + 1
							else:
								i = i + 1
			
					match_pola_8 = re.match(r'<start>\s.+\s(adalah)\s.+', kalimat_tag)
					if match_pola_8:
						words = []
						tags = []
						index = 0
						index_adalah = -1
						for word, tag in text.pos_tags:
							words.append(word)
							tags.append(tag)
							if word == "adalah":
								index_adalah = index
							index = index + 1

						parts = re.split(' adalah ', kalimat_tag, 1)

						#mendapatkan hipernim
						right = parts[1].replace(" <end>", "")
						tokens_right = nltk.word_tokenize(right.lower())
						bigram_right = list(nltk.bigrams(tokens_right)) 
						for (a,b) in bigram_right:
							tokens_right.append(a + " " + b)
						for temp in tokens_right:
							f_hipernim.write(nama_provinsi + "\t" + temp + "\n")
						kategori = getKategoriIndex(tokens_right)

						#mengambil hiponim
						words_hyponym = words[:index_adalah]
						tags_hyponym = tags[:index_adalah]
						#mendapatkan PROPN atau NOUN berurutan
						start_index = -1
						end_index = -1
						i = 0
						while i < len(tags_hyponym):
							if tags_hyponym[i] == "PROPN":
								start_index = i
								j = i + 1
								while j < len(tags_hyponym) and tags_hyponym[j] == "PROPN":
									j = j + 1
								end_index = j - 1
								if start_index == end_index:
									hyponym = words_hyponym[start_index]
									if hyponym in map_entity_ner:
										print (hyponym, file=f_filterNER)
									if kategori != -1 and hyponym not in map_entity_ner:
										f_relasi.write(str(kategori) + "\t" + str(8) + "\t" + kalimat + "\t" + right + "\t" + hyponym + "\t" + nama_provinsi + "\n")
										if not hyponym in array_all_heritage_sentences[kategori]:
											array_all_heritage_sentences[kategori].append(hyponym)
								else:
									start = start_index
									end = end_index
									hyponym = ""
									while start <= end:
										hyponym = hyponym + " " + words_hyponym[start]
										start = start + 1
									hyponym = hyponym.strip()
									if hyponym in map_entity_ner:
										print (hyponym, file=f_filterNER)
									
									if kategori not in arr_kategori_awalan and kategori != -1 and hyponym not in map_entity_ner:
										f_relasi.write(str(kategori) + "\t" + str(8) + "\t" + kalimat + "\t" + right + "\t" + hyponym + "\t" + nama_provinsi + "\n")
										if not hyponym in array_all_heritage_sentences[kategori]:
											array_all_heritage_sentences[kategori].append(hyponym)
								i = j + 1
							else:
								i = i + 1

					match_pola_9 = re.match(r'<start>\s.+\s(merupakan)\s.+', kalimat_tag)
					if match_pola_9:
						words = []
						tags = []
						index = 0
						index_merupakan = -1
						for word, tag in text.pos_tags:
							words.append(word)
							tags.append(tag)
							if word == "merupakan":
								index_merupakan = index
							index = index + 1

						parts = re.split(' merupakan ', kalimat_tag, 1)

						#mendapatkan hipernim
						right = parts[1].replace(" <end>", "")
						tokens_right = nltk.word_tokenize(right.lower())
						bigram_right = list(nltk.bigrams(tokens_right)) 
						for (a,b) in bigram_right:
							tokens_right.append(a + " " + b)
						for temp in tokens_right:
							f_hipernim.write(nama_provinsi + "\t" + temp + "\n")
						kategori = getKategoriIndex(tokens_right)

						#mengambil hiponim
						words_hyponym = words[:index_merupakan]
						tags_hyponym = tags[:index_merupakan]
						#mendapatkan PROPN atau NOUN berurutan
						start_index = -1
						end_index = -1
						i = 0
						while i < len(tags_hyponym):
							if tags_hyponym[i] == "PROPN":
								start_index = i
								j = i + 1
								while j < len(tags_hyponym) and tags_hyponym[j] == "PROPN":
									j = j + 1
								end_index = j - 1
								if start_index == end_index:
									hyponym = words_hyponym[start_index]
									if hyponym in map_entity_ner:
										print (hyponym, file=f_filterNER)
									if kategori != -1 and hyponym not in map_entity_ner:
										f_relasi.write(str(kategori) + "\t" + str(9) + "\t" + kalimat + "\t" + right + "\t" + hyponym + "\t" + nama_provinsi + "\n")
										if not hyponym in array_all_heritage_sentences[kategori]:
											array_all_heritage_sentences[kategori].append(hyponym)
								else:
									start = start_index
									end = end_index
									hyponym = ""
									while start <= end:
										hyponym = hyponym + " " + words_hyponym[start]
										start = start + 1
									hyponym = hyponym.strip()
									if hyponym in map_entity_ner:
										print (hyponym, file=f_filterNER)
									kategori_awalan = match_awalan(hyponym)
									
									if kategori not in arr_kategori_awalan and kategori != -1 and hyponym not in map_entity_ner:
										f_relasi.write(str(kategori) + "\t" + str(9) + "\t" + kalimat + "\t" + right + "\t" + hyponym + "\t" + nama_provinsi + "\n")
										if not hyponym in array_all_heritage_sentences[kategori]:
											array_all_heritage_sentences[kategori].append(hyponym)
								i = j + 1
							else:
								i = i + 1

					#menggunakan awalan jika tidak cocok dengan pattern manapun
					words = []
					tags = []
					for word, tag in text.pos_tags:
						words.append(word)
						tags.append(tag)

					#mendapatkan PROPN atau NOUN berurutan
					start_index = -1
					end_index = -1
					i = 0
					while i < len(words):
						if tags[i] == "PROPN" or tags[i] == "NOUN":
							start_index = i
							j = i + 1
							while j < len(tags) and (tags[j] == "PROPN" or tags[j] == "NOUN"):
								j = j + 1
							end_index = j - 1
							if start_index < end_index:
								start = start_index
								end = end_index
								entity = ""
								while start <= end:
									entity = entity + " " + words[start]
									start = start + 1
								entity = entity.strip()
								if entity in map_entity_ner:
									print (entity, file=f_filterNER)
								f_hiponim.write(nama_provinsi + "\t" + entity + "\n")
								kategori_awalan = match_awalan(entity)
								if kategori_awalan != -1 and "PROPN" in tags[start_index:end_index+1]:
									f_awalan.write(str(kategori_awalan) + "\t" + entity + "\t" + kalimat + "\t" + nama_provinsi + "\n")
									if not entity in array_all_heritage_sentences[kategori_awalan]:
										array_all_heritage_sentences[kategori_awalan].append(entity)
							i = j + 1
						else:
							i = i + 1
		except:
			pass
	# memasukkan kalimat-kalimat yang diduga mengandung entitas budaya
	fillProvinceHeritageMap(provinsi_heritage, nama_provinsi, array_all_heritage_sentences)
	printToFileAllHeritage("sentences", provinsi_heritage, nama_provinsi)
	make_empty(array_all_heritage_sentences)
	print ("Free texts in " + nama_provinsi + " done... ")
printStatisticTable("sentences", array_nama_provinsi, provinsi_heritage)
f_relasi.close()
f_hipernim.close()
f_hiponim.close()