#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function
import codecs
from list_function import getURIKategori, getPredicateObjectProvince, getPredicateObjectCity

directory_names = ["aksara", "alat_musik", "bahasa", "benteng", "candi", "cerita_rakyat", "istana", "kerajaan", "kolam", "komunitas_adat", "kuil", "lagu", "makam", "makanan", "masjid", "motif", "musik", "naskah", "ornamen", "pakaian", "patung", "permainan", "petirtaan", "prasasti", "pura", "ritual", "rumah", "seni_pertunjukkan", "senjata", "situs", "suku", "tari", "tradisi_lisan"]

file_budaya = open ("budaya.ttl", "w")
print ("@prefix wd:   <http://www.wikidata.org/entity/> .", file=file_budaya)
print ("@prefix bkb:   <https://budayakb.cs.ui.ac.id/ns#> .", file=file_budaya)
print ("@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .", file=file_budaya)
print ("@prefix bkbr:  <https://budayakb.cs.ui.ac.id/resource/> .", file=file_budaya)
print ("@prefix owl:  <http://www.w3.org/2002/07/owl#>  .", file=file_budaya)

with open('df_merge_budayakb.csv', 'r') as coll:
	i = 0
	for linecoll in coll:
		if i == 0:
			i = i + 1
		else:	
			#mendapatkan nama kategori, entitas, provinsi, dan kab/kota
			partscoll = linecoll.split(",")
			kategori = directory_names.index(partscoll[0].strip())
			entity = partscoll[1].strip()
			#membuat statement kategori dari entity
			print ("bkbr:" + entity.replace(" ", "_") + " a bkb:" + getURIKategori(kategori) + " .", file=file_budaya)
			provinsi = partscoll[2].strip()
			#membuat statement pembentukan resource provinsi
			print ("bkbr:" + provinsi.replace(" ", "_") + " a bkb:Provinsi .", file=file_budaya)
			kabupaten_kota = partscoll[3].strip()
			#membuat statement provinsi dari entity
			print ("bkbr:" + entity.replace(" ", "_") + " " + getPredicateObjectProvince(kategori, provinsi) + " .", file=file_budaya)
			#membuat statement kabupaten/kota dari entity
			if kabupaten_kota != "None":
				#membuat statement pembentukan resource kabupaten/kota
				print ("bkbr:" + kabupaten_kota.replace(" ", "_") + " a bkb:KabupatenKota .", file=file_budaya)
				print ("bkbr:" + entity.replace(" ", "_") + " " + getPredicateObjectCity(kategori, kabupaten_kota) + " .", file=file_budaya)
				print ("bkbr:" + kabupaten_kota.replace(" ", "_") + " bkb:province bkbr:" + provinsi.replace(" ", "_") + " .", file=file_budaya)
			id_wiki = partscoll[4].strip()
			if id_wiki != "None":
				print ("bkbr:" + entity.replace(" ", "_") + " owl:sameAs wd:" + id_wiki + " .", file=file_budaya)

				