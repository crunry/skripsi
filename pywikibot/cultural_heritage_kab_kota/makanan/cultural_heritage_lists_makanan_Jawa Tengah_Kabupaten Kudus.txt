Pusat Kuliner Matahari, di Getaspejaten (di Halaman Plaza Kudus) | Wisata kuliner - Pariwisata
Pusat Kuliner Sempalan Indah[2], di Jati Kulon | Wisata kuliner - Pariwisata
Taman Bojana, di Getaspejaten (di utara Alun - alun simpang 7) | Wisata kuliner - Pariwisata
Soto Kudus | Masakan - Kuliner khas Kudus
Lentog Tanjung | Masakan - Kuliner khas Kudus
Garang Asem | Masakan - Kuliner khas Kudus
Sate Kerbau Kudus | Masakan - Kuliner khas Kudus
Pindang Kerbau | Masakan - Kuliner khas Kudus
Opor Bakar Sunggingan | Masakan - Kuliner khas Kudus
Sambal Jeruk Pamelo | Sambal - Kuliner khas Kudus
Sambal Cengkeh | Sambal - Kuliner khas Kudus
Kopi Jetak | Minuman - Kuliner khas Kudus
Wedang Alang-Alang Kudus | Minuman - Kuliner khas Kudus
Wedang Pejuh | Minuman - Kuliner khas Kudus
Jenang Kudus | Oleh-Oleh - Kuliner khas Kudus
Kacang Bawang | Oleh-Oleh - Kuliner khas Kudus
Jambu Bol | Oleh-Oleh - Kuliner khas Kudus
Duku Sumber | Oleh-Oleh - Kuliner khas Kudus
Bordir Kudus | Oleh-Oleh - Kuliner khas Kudus
Batik Kudus | Oleh-Oleh - Kuliner khas Kudus
