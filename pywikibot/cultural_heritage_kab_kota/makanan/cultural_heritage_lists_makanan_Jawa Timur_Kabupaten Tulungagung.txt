Sate dan Gule Kambing, Sate Tulungagung mirip dengan sate lainnya dan tampak sederhana, terdiri dari daging kambing yang ditusuk dalam sujen (tusuk sate) bambu, disajikan dengan bumbu kecap yang diberi merica dan petis, serta ditaburi dengan irisan bawang merah, di beberapa warung ditambah irisan daun jeruk, berbeda dengan tampilan Sate di kabupaten Trenggalek (Sate Bendo) yang dalam penyajiannya ditaburi kecambah sama seperti daerah Nganjuk, tidak seperti sate Madura dan sate Ponorogo dan Kediri, yang bumbu-nya mengandung kacang, Sehingga rasanya memang khas Tulungagung-an, pada dasarnya perbedaan rasa ini dikarenakan proses bakarnya dicelupkan dalam kuah gule dan pemakaian kecap manis tradisional merk kuda khas tulungagung-an. | Wisata Kuliner
Nasi Lodho Tulungagung, sebenarnya kuliner ini mirip dengan kare ayam, hanya saja ayamnya dipanggang/diasap terlebih dulu dan disajikan bersama nasi/tiwul (tiwul adalah nasi yang terbuat dari gaplek/singkong) dengan pelengkap gudhangan (kudapan) sayur-sayuran, namun dalam perkembangannya lebih banyak yang disajikan (warung kaki lima) serupa dengan kare ayam. Lodho Tulungagung dibedakan dalam 2 genre,yaitu Lodho kuah kental dan encer, kekentalannya berasal dari konsentrasi santan, biasanya rasanya pedas,ayamnya ayam kampung. | Wisata Kuliner
Sredek, Makanan yang terbuat dari gethuk singkong, kemudian digoreng. Biasa dimakan dengan tempe goreng dan cabe mentah (sebagai lalap), adalah makanan khas Tulungagung selatan. | Wisata Kuliner
Kemplang, makanan yang terbuat dari ketela yang diparut dikasih bumbu-bumbu dibentuk pipih diatasnya dikasih kacang lotho lalu di goreng itu juga makanan khas tulungagung | Wisata Kuliner
Emping Melinjo, makanan ini terbuat dari biji belinjo yang dipipihkan dan kemudian dijemur seperti kerupuk. | Wisata Kuliner
Kerupuk Gadung, kuliner yang untuk saat ini pembuatannya hanya dikuasai oleh sedikit orang (umumnya orang tua) karena pengolahannya harus diperam dulu menggunakan abu untuk menghilangkan kandungan getah gadung agar tidak menyebabkan efek mabuk/pusing ketika dimakan. | Wisata Kuliner
Soto Ayam Kampung Tulungagung warung soto dengan aroma rempah yang kuat dan kemiri sebagai penguat rasa banyak ditemui disekitaran Kecamatan Kauman dan Kecamatan Gondang | Wisata Kuliner
Nasi pecel Tulungagung, nasi pecel dengan karakter sambal pecel seperti di daerah Kabupaten Blitar, yang membedakan dengan pecel dari daerah lain seperti Madiun/Ponorogo adalah karakter sambal kacang yang pedas manis (karena penambahan gula jawa/gula aren) serta aroma daun jeruk yang kuat. | Wisata Kuliner
Sompil, Lontong diiris kemudian disiram dengan sayur lodeh (umumnya lodeh kacang) dan diatasnya ditambahi dengan bubuk kedelai yang gurih-manis. | Wisata Kuliner
Lopis, makanan seperti lontong biasanya dicampur cenil, kicak atau gethuk dikasih larutan gula merah | Wisata Kuliner
Cenil Yang dibuat dari singkong yang diolah melalui proses ditumbuk/digiling yang biasanya juga dibuat bersama Kicak, disajikan dengan parutan kelapa muda dan disiram dengan gula jawa/gula aren cair. | Wisata Kuliner
Kerupuk Rambak Tulungagung, kerupuk yang terbuat dari kulit sapi/kerbau serupa kerupuk jangek di Padang-Sumatra Barat namun dengan karakter yang lebih renyah, sentra industri kerupuk ini ada di seputaran Botoran Panggungrejo kota, Sembung. | Wisata Kuliner
Gethuk, singkong rebus yang dihaluskan dengan cara ditumbuk bersama gula jawa/ gula aren dan disajikan dengan taburan parutan kelapa diatasnya. | Wisata Kuliner
Srondeng, parutan kelapa yang digoreng dengan dibumbui sedemikian rupa sampai berwarna merah kecoklatan, kadang-kadang buat campuran dendeng sapi | Wisata Kuliner
Jenang Syabun, jenang yang diolah dari beras ketan menjadi serupa dodol dengan penggabungan karakter rasa manis dari dua macam gula, gula jawa dan gula pasir,jenang ini mempunyai tektur lembut namun kenyal dan tidak lengket,originalnya jenang initidakmenggunakan pengawet,sehingga jarang dipajang ditoko,jika berminat disarankan datang ke pabriknya di desa Botoran. | Wisata Kuliner
Jenang Grendol, makanan terbuat dari tepung kanji, biasanya disajikan bersama dengan Jenang Baning yang terbuat dari tepung beras serta Jenang Ketan dari bubur ketan hitam. Secara terpisah Jenang Grendol disajikan dengan kuah santan karena karakter jenang itu sendiri yang sudah manis namun apabila dicampur akan diberikan kuah gula jawa/gula aren yang umum disebut Juruh. | Wisata Kuliner
Geti, adalah nuget terbuat dari wijen kadang-kadang dicampur kacang yang dimasak dengan gula sehingga memunculkan sensasi rasa yang manis-gurih. | Wisata Kuliner
Kopi Cethe, ampas kopi yang dijadikan bahan pengoles rokok agar memiliki aroma yang lebih sedap. | Wisata Kuliner
Punten Pecel, Punten serupa dengan Jadah cuma bedanya kalau Jadah terbuat dari bahan ketan sementara Punten dari bahan beras yang ditanak dengan santan gurih dan kemudian dijelu atau ditumbuk pelan dan umumnya ditambah parutan kelapa muda sehingga tercipta adonan kenyal dan gurih yang biasanya disajikan dengan pecel. | Wisata Kuliner
Brondong Ketan, di Tulungagung umumnya disebut Bipang, dengan mengolah berondong dari beras ketan yang diolah dengan gula. | Wisata Kuliner
Capar Tape, atau disebut tape pecel yang terbuat dari tape singkong (umumnya putih) dan disiram sayur pecel bahkan biasanya juga ditambahkan mentimun rebus. | Wisata Kuliner
Glondhong Juruh,asli Sambitan, terbuat dari kukusan ketela pohon disiram juruh kental atau dibuat dengan memasukkan singkong kedalam ke jadi/wajan besar tempat orang memasak gula jawa/gula tebu sehingga menjadi manis, kadang-kadang disebut juga Cimplung yang mungkin karena dibuat dengan nyemplung/memasukan singkong ke wadah pengolahan gula. | Wisata Kuliner
Sego Bantingan, nasi bungkus yang dijual secara murah meriah, pelengkapnya sederhana (lauk standar dan sambal/keringan) dan apabila ingin menambahkan sayur atau lauk ada disiapkan secara terpisah. | Wisata Kuliner
Gembrot, kuliner khas yang terbuat dari beberapa jenis dedaunan yang dicampur dengan parutan kelapa yang telah dibumbui sedemikian rupa kemudian dibungkus dengan daun kelapa dan dikukus, kadang-kadang didalamnya juga ditambahkan sejenis ikan sungai atau udang. | Wisata Kuliner
Gathot, makanan yang terbuat dari singkong yang direndam air garam kemudian dijemur hingga kering menjadi Gaplek, gaplek yang dicacah/diiris tipis apabila ditanak menjadi Gathot dan disajikan dengan parutan kelapa muda, sementara itu Gaplek yang ditumbuk menjadi Tiwul dan ditanak sebagai pengganti nasi | Wisata Kuliner
Klethek, klethek merupakan makanan yang terbuat dari singkong yang dalam pengolahannya dicampur dengan bumbu-bumbu lainnya, seperti terasi dan kedelai. Klethek mirip dengan keripik singkong hanya saja dalam pemasakannya klethek digoreng sedikit lebih lama. | Wisata Kuliner
Wahono, mantan Ketua MPR-RI (1992-1997) | Wisata Kuliner
Brigjen Pol (Purn) Dra. Rumiyah Kartoredjo, S.Pd. (Kepala Kepolisian Daerah Banten 2008-2010) | Wisata Kuliner
Sri Bintang Pamungkas, politikus | Wisata Kuliner
Alfa Isnaeni, Politikus, Pimpinan Pusat Gerakan Pemuda Ansor, Kepala BANSER Nasional (KASATKORNAS BANSER), KONI Kab. Tulungagung | Wisata Kuliner
Ali Masykur Musa, politikus | Wisata Kuliner
Suroso @Mbah_wilist'z, Juru Kunci Gunung Wilis | Wisata Kuliner
Sri Somantri, pakar hukum tata negara Universitas Padjadjaran | Wisata Kuliner
Yogi Sugito, rektor Universitas Brawijaya (2006 - 2010) | Wisata Kuliner
Irjen Pol Drs. Mudji Waluyo, SH, MM. (Kepala Kepolisian Daerah Sulawesi Selatan 2012-sekarang) | Wisata Kuliner
Inten Suweno, Mantan menteri Peranan Wanita (UPW) | Wisata Kuliner
Triyogi Yuwono, Guru besar, Rektor (2011-2015) ITS-Surabaya | Wisata Kuliner
Pangeran Adipati Soejono, politikus Belanda | Wisata Kuliner
KI Dalang Moerdi Kondo Moerdiyat, Sesepuh Tulungagung Yang Paling dihormati tahun 90-an | Wisata Kuliner
Yongki Ariwibowo, Pesepak bola timnas Indonesia AFF cup 2010 | Wisata Kuliner
Prof.Dr.H.Suparno, Rektor Universitas Negeri Malang 2007-2014 | Wisata Kuliner
Ardian Syaf, Komikus DC Comics (2012-sekarang), Marvel Comics (2009), dan Dynamite (2007) | Wisata Kuliner
Arsyad Yusgiantoro, Pesepak bola Persegres Gresik United, Danone Cup | Wisata Kuliner
Rumah Sakit Pemerintah: 1 | Wisata Kuliner
Rumah Sakit POLRI: 1 | Wisata Kuliner
Rumah Sakit swasta:

RSU Era Medika
RSU Madinah
RSU Satiti
RSU Muhammadiyah
RSUI Orpeha
RSU Putra Waspada
RSIA Fausiyah
RSIA Amanda
RSIA Cita Sehat

 | Wisata Kuliner
RSU Era Medika | Wisata Kuliner
RSU Madinah | Wisata Kuliner
RSU Satiti | Wisata Kuliner
RSU Muhammadiyah | Wisata Kuliner
RSUI Orpeha | Wisata Kuliner
RSU Putra Waspada | Wisata Kuliner
RSIA Fausiyah | Wisata Kuliner
RSIA Amanda | Wisata Kuliner
RSIA Cita Sehat | Wisata Kuliner
Pelita Indah : Trenggalek - Kertosono - Surabaya | Wisata Kuliner
Harapan Jaya AC Tarif Biasa : Tulungagung - Kertosono / Pare - Surabaya | Wisata Kuliner
Harapan Jaya PATAS Biasa : Tulungagung - Kertosono - Mojokerto - Surabaya | Wisata Kuliner
Harapan Jaya PATAS via TOL : Tulungagung - Kediri - Surabaya | Wisata Kuliner
Harapan Jaya Bus Malam : Tulungagung - Solo - Semarang - Jakarta - Lampung - Palembang | Wisata Kuliner
Damri Perintis : Tulungagung - Pagerwojo - Bendungan - Sooko - Pulung - Ponorogo | Wisata Kuliner
Gunung Harta : Tulungagung - Denpasar | Wisata Kuliner
Restu Mulya : Tulungagung - Denpasar | Wisata Kuliner
Antar Lintas Sumatera : Tulungagung - Medan | Wisata Kuliner
Bagong : Trenggalek - Tulungagung - Blitar - Malang | Wisata Kuliner
Setiawan : Trenggalek - Tulungagung - Denpasar | Wisata Kuliner
MTrans : Ponorogo - Trenggalek - Tulungagung - Blitar - Malang - Denpasar | Wisata Kuliner
Harapan Baru : Trenggalek - Banyuwangi | Wisata Kuliner
Gajayana : Malang - Jakarta Gambir | Wisata Kuliner
Malabar : Malang - Bandung | Wisata Kuliner
Malioboro : Malang - Yogyakarta | Wisata Kuliner
Singasari : Blitar - Jakarta Pasar Senen | Wisata Kuliner
Majapahit : Malang - Jakarta Pasar Senen | Wisata Kuliner
Matarmaja : Malang - Jakarta Pasar Senen | Wisata Kuliner
Brantas : Blitar - Jakarta Pasar Senen | Wisata Kuliner
Kahuripan : Blitar - Bandung Kiaracondong | Wisata Kuliner
Rapih Dhoho : Blitar - Surabaya Kota | Wisata Kuliner
Dhoho Penataran : Surabaya Kota - Tulungagung - Malang - Surabaya Kota | Wisata Kuliner
KOMPAS (Komunitas Pecinta Alam Semesta) | Wisata Kuliner
Pendaki Gunung Tulungagung | Wisata Kuliner
Tulungagung Graffiti Crew | Wisata Kuliner
Vanshead Tulungagung | Wisata Kuliner
Tulungagung Skate Population | Wisata Kuliner
BMX tulungagung | Wisata Kuliner
Tulungagung Anime Community | Wisata Kuliner
Tulungagung Daisuki | Wisata Kuliner
Love Live Tulungagung | Wisata Kuliner
Tulungagung Muda | Wisata Kuliner
Svkatani Crew | Wisata Kuliner
Peon Community | Wisata Kuliner
