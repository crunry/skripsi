Momoh | Makanan - Kuliner Khas Kendal
Sate Bumbon | Makanan - Kuliner Khas Kendal
Rica Rica Menthog | Makanan - Kuliner Khas Kendal
Bebek Ijo | Makanan - Kuliner Khas Kendal
Soto Kendal | Makanan - Kuliner Khas Kendal
Pecel Kembang Turi | Makanan - Kuliner Khas Kendal
Bandeng Tanpa Duri (Tandu) | Makanan - Kuliner Khas Kendal
Mangut Lele | Makanan - Kuliner Khas Kendal
Brongkos | Makanan - Kuliner Khas Kendal
Telur Ikan Mimi | Makanan - Kuliner Khas Kendal
Panggang Ikan Klayar | Makanan - Kuliner Khas Kendal
Mangut Kepala Ikan Manyung | Makanan - Kuliner Khas Kendal
Bir Jawa | Minuman - Kuliner Khas Kendal
Kopi Cacaban | Minuman - Kuliner Khas Kendal
Kerupuk Petis | Oleh oleh - Kuliner Khas Kendal
Kerupuk Rambak | Oleh oleh - Kuliner Khas Kendal
Kerupuk Tayammum (Goreng Wedi) | Oleh oleh - Kuliner Khas Kendal
Spesial Gulali Khas Kendal | Oleh oleh - Kuliner Khas Kendal
Olahan Jambu Getas Merah | Oleh oleh - Kuliner Khas Kendal
Emping Bandeng | Oleh oleh - Kuliner Khas Kendal
Abon Bandeng Cabut Duri | Oleh oleh - Kuliner Khas Kendal
Stik Balado Pedas | Oleh oleh - Kuliner Khas Kendal
Kerupuk Duri Bandeng | Oleh oleh - Kuliner Khas Kendal
Krecek Ketan Bandeng Cabut Duri | Oleh oleh - Kuliner Khas Kendal
Sumpil | Oleh oleh - Kuliner Khas Kendal
Ondal Andil | Oleh oleh - Kuliner Khas Kendal
