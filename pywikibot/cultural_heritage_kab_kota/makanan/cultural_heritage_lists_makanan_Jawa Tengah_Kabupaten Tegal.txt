Teh poci - Teh poci diseduh dalam poci tanah liat dan diminum dengan gula batu. Istilah teh poci adalah wasgitel artinya wangi, panas, sepet, legi, lan kentel, Kabupaten Tegal hingga saat ini dikenal sebagai sentra penghasil teh. | Makanan khas - Kuliner
Kupat sambel tahu lengko Mbah Pa'ong - Makanan Khas dari daerah Margasari. Terbuat dari beras alami yang dibungkus dengan daun kelapa dengan bentuk seperti urung. Dan dicampur dengan tauge, potongan tahu aci, disiram sambal kacang ekstra pedas dan ditaburi bawang goreng serta kerupuk mie kuning. Kupat Sambel Tahu Lengko dari generasi penerus Mbah Pa'ong masih dapat ditemui di kios Wak Dakum di kompleks Pasar Margasari | Makanan khas - Kuliner
Mendoan - Tempe goreng dilapisi tepung dengan bumbu. digoreng setengah matang. Sebagai teman minum Teh Poci, dihidangkan dengan kecap dicampur cabe rawit. Mendoan juga bisa ditemukan di wilayah Banyumasan. | Makanan khas - Kuliner
Sega lengko - Nasi dengan bahan pelengkap seperti tempe, tahu yang diiris dadu, tauge, kol mentah, dan sambal kacang beserta kerupuk. | Makanan khas - Kuliner
Soto tegal - Soto ayam/babat khas Tegal dengan bumbu tauco dan tauge. | Makanan khas - Kuliner
Kemronyos - Sate khas Tegal | Makanan khas - Kuliner
Kupat glabed | Makanan khas - Kuliner
Kupat bongkok - Kupat asal Desa Bongkok | Makanan khas - Kuliner
Sate bebek majir | Makanan khas - Kuliner
Sate blengong - Sate yang dagingnya berasal dari hewan hasil perkawinan silang itik dan mentok (blengong). | Makanan khas - Kuliner
Nasi ponggol | Makanan khas - Kuliner
Nasi ponggol setan (pongset) | Makanan khas - Kuliner
Sate tegal - Sate kambing muda dengan bumbu sambal kecap | Makanan khas - Kuliner
Nasi bogana - Nasi sejenis nasi megono | Makanan khas - Kuliner
Olos - Jajanan ini terbuat dari tepung yang dibentuk bulat, kemudian diisi dengan kembang kol, bawang, dan cabai rawit. Lalu digoreng hingga adonan bulatan mengeras. Jajanan ini berasal dari Jatirawa. Asal mula jajanan yaitu, saat si penemu jajanan ini berawal berjualan risoles di Jatirawa, anak-anak kecil tidak menyebut risoles melainkan oles, sehingga jajanan ini pun dinamakan olos. Jajanan ini awalnya berwujud seperti risoles pada umumnya. Namun, karena ada pembeli tidak suka wortel maupun tauge. Akhirnya hanya berisi kol dan cabai saja. | Jajanan gorengan dan lauk - Jajanan khas Tegal - Kuliner
Petis - Jajanan yang terbuat dari sisa perasan kedelai (gembus / amprut) yang dihaluskan dan diberi cabai rawit kemudian dimasak. Petis juga bisa dimasak dengan diberi tulang ayam/kambing agar lebih nikmat. | Jajanan gorengan dan lauk - Jajanan khas Tegal - Kuliner
Kerupuk Anthor - Kerupuk dari ketela yang goreng dengan ampas kelapa (pasir kaligung). Untuk menikmatinya harus menyediakan minuman. Karena apabila memakan kerupuk ini tenggorokan akan cepat terasa serak. | Jajanan gorengan dan lauk - Jajanan khas Tegal - Kuliner
Tahu kuping - Tahu Kuning yang diisi adonan tepung sagu berbentuk kuping kemudian digoreng | Jajanan gorengan dan lauk - Jajanan khas Tegal - Kuliner
Rujak teplak - Rujak dengan sambal khas tapenya. | Jajanan gorengan dan lauk - Jajanan khas Tegal - Kuliner
Tahu aci | Jajanan gorengan dan lauk - Jajanan khas Tegal - Kuliner
Tahu plethok | Jajanan gorengan dan lauk - Jajanan khas Tegal - Kuliner
Opak - Makanan bundar tipis dari singkong, bisa dimakan dengan sambal. | Jajanan gorengan dan lauk - Jajanan khas Tegal - Kuliner
Pilus - Makanan ringan (snack) dari tepung terigu. | Jajanan gorengan dan lauk - Jajanan khas Tegal - Kuliner
Kacang asin bogares - Makanan ringan dari desa Bogares Kidul | Jajanan gorengan dan lauk - Jajanan khas Tegal - Kuliner
Gejos | Jajanan gorengan dan lauk - Jajanan khas Tegal - Kuliner
Martabak Lebaksiu | Jajanan kue - Jajanan khas Tegal - Kuliner
Kue lolos | Jajanan kue - Jajanan khas Tegal - Kuliner
Kue bongko | Jajanan kue - Jajanan khas Tegal - Kuliner
Kue talam | Jajanan kue - Jajanan khas Tegal - Kuliner
Semprong | Jajanan kue - Jajanan khas Tegal - Kuliner
Kecepit | Jajanan kue - Jajanan khas Tegal - Kuliner
Jenang / dodol glempang | Jajanan kue - Jajanan khas Tegal - Kuliner
Gemblong kocar-kacir | Jajanan kue - Jajanan khas Tegal - Kuliner
Moci - Budaya minum teh sebagai teman ngobrol, biasanya dilakukan beramai-ramai. | Budaya khas Tegal - Kuliner
Kesenian asli Kota Tegal adalah tari endel, sintren, dan balo-balo. Ibu Sawitri merupakan generasi pertama penari endel. | Budaya khas Tegal - Kuliner
