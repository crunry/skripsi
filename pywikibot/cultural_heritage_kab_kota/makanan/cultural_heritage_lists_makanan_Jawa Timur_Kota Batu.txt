Pos Ketan Legenda | Masakan - Kuliner
Sate Kelinci | Masakan - Kuliner
Soto | Masakan - Kuliner
Bakso Batu | Masakan - Kuliner
Sego Bancakan | Masakan - Kuliner
Lalap Ikan Wader | Masakan - Kuliner
Ketan Manis (yaitu jajanan pasar, terdiri dari ketan, bubuk kelapa dan gula manis) | Jajanan pasar - Kuliner
Tape Ketan Hitam (yaitu bisa ditemukan di daerah Cangar, yang dapat menghangatkan tubuh) | Jajanan pasar - Kuliner
Tahu Kentaki Dhigadho (yaitu gorengan tahu alami, dengan rasa yang khas rempah-rempah pilihan) | Jajanan pasar - Kuliner
Es Krim Milco (yaitu es krim buatan rumahan merk MILCO khas Batu) | Minuman - Kuliner
Angsle (sejenis kolak dengan ketan dan serabi juga petulo yang sangat nikmat dengan suasana dingin Kota Batu) | Minuman - Kuliner
Susu KUD Kota Batu | Minuman - Kuliner
Berbagai produk apel, termasuk: sari apel, jenang dan dodol apel, cuka apel | Oleh-oleh - Kuliner
Berbagai keripik: keripik singkong, kentang, dan aneka buah lainnya | Oleh-oleh - Kuliner
Berbagai sari buah: Sari buah apel, dan lainnya | Oleh-oleh - Kuliner
