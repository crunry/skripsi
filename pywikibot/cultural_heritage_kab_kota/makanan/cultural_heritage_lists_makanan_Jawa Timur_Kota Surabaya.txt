Lontong Balap | Masakan - Kuliner
Tahu Tek | Masakan - Kuliner
Krengsengan | Masakan - Kuliner
Tempe Penyet | Masakan - Kuliner
Lontong Mie | Masakan - Kuliner
Kupang Lontong | Masakan - Kuliner
Rawon | Masakan - Kuliner
Tahu Campur | Masakan - Kuliner
Sop Kikil | Masakan - Kuliner
Sup buntut | Masakan - Kuliner
Kari Kambing | Masakan - Kuliner
Bakwan Surabaya | Masakan - Kuliner
Nasi Sayur | Masakan - Kuliner
Nasi Goreng Jawa | Masakan - Kuliner
Bakso | Masakan - Kuliner
Pecel Semanggi | Salad - Kuliner
Rujak Cingur | Salad - Kuliner
Urap | Salad - Kuliner
Gado-gado | Salad - Kuliner
Pecel | Salad - Kuliner
Roti Perut Ayam | Jajanan - Kuliner
Getas (ketan putih / hitam yang digoreng lalu diberi taburan gula bubuk) | Jajanan - Kuliner
Kue Leker | Jajanan - Kuliner
Kue Lapis Surabaya | Jajanan - Kuliner
Bikang (Carabika) | Jajanan - Kuliner
Jongkong | Jajanan - Kuliner
Onde-onde Surabaya | Jajanan - Kuliner
Lupis | Jajanan - Kuliner
Almond Crispy Cheese | Jajanan - Kuliner
Cakue | Jajanan - Kuliner
Roti Goreng | Jajanan - Kuliner
Angsle | Minuman - Kuliner
Ronde | Minuman - Kuliner
Tahwa | Minuman - Kuliner
STMJ (Susu Telur Madu Jahe) | Minuman - Kuliner
