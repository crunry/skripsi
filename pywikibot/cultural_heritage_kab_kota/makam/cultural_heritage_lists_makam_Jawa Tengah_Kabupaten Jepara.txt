Makam Syekh Siti Jenar (Sunan Jepara), di Balong | Wisata religi - Pariwisata
Makam Mbah Roboyo, di Robayan | Wisata religi - Pariwisata
Makam Datuk Gunardi, di Singorojo | Wisata religi - Pariwisata
Makam Habib Ali, di Mayong | Wisata religi - Pariwisata
Makam Syeh Abu Bakar, di Pulau Panjang | Wisata religi - Pariwisata
Makam Ki Gede, di Bangsri | Wisata religi - Pariwisata
Makam Syeh Amir Hasan (Sunan Nyamplungan), di Karimunjawa | Wisata religi - Pariwisata
Makam Mbah Pakisaji, di Potroyudan | Wisata religi - Pariwisata
Makam Ronggo Kusumo, di Manyargading | Wisata religi - Pariwisata
Makam Mbah Datuk Subuh, di Sidigede | Wisata religi - Pariwisata
Makam Habib Sodiq (Yek Nde) dan KH Noor Ahmad SS, di Kriyan | Wisata religi - Pariwisata
Makam Assayyid Thoyyib Thohir dan Syaikh Syamsuri, di (Penagon, Nalumsari, Jepara) | Wisata religi - Pariwisata
Makam Pangeran Syarif dan Mbah Jenggolo, di Saripan | Wisata religi - Pariwisata
Makam Sunan Hadirin dan Ratu Kalinyamat serta Raden Abdul Jalil, di Mantingan | Wisata religi - Pariwisata
Makam KH. Ahmad Cholil, di Bakalan | Wisata religi - Pariwisata
