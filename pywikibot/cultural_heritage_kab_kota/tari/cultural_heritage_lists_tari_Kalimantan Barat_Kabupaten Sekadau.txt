Tari Tundet | Tari - Kesenian
Tari Pinggan, tersebar di daerah Belitang Hilir, Belitang Tengah dan Belitang Hulu | Tari - Kesenian
Tari Pala, tersebar di Belitang Hilir, Kampung Sungai Mirah Desa batu Ancau. Nara Sumber Ibu Jeriah ] | Tari - Kesenian
Tari Pedang, tersebar di Belitang Hilir, Belitang Tengah dan Belitang Hulu ] | Tari - Kesenian
Tari Ngajat Temuai Datai, merupakan bentuk tari penyambutan terhadap tamu yang datang dan tersebar di Belitang Hilir, Tengah dan Hulu | Tari - Kesenian
Tari Sampe, tersebar di daerah Sekadau Hilir, tepatnya di seberang Sungai Kapuas (Dayak Ketunggau sesat wilayah Kedah), merupakan tarian penyambutan tamu, iringan pengantin dan pembukaan acara adat setempat | Tari - Kesenian
