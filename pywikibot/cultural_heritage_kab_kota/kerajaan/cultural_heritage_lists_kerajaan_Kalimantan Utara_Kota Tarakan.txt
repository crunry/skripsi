Dialek bahas Tidung Malinau | Era Kerajaan Tidung - Sejarah
Dialek bahasa Tidung Sembakung. | Era Kerajaan Tidung - Sejarah
Dialek bahas Tidung Sesayap. | Era Kerajaan Tidung - Sejarah
Dialek bahas Tidung Tarakan yang biasa pula disebut Tidung Tengara yang kebanyakan bermukim di daerah air asin. | Era Kerajaan Tidung - Sejarah
Benayuk dari sungai Sesayap, Menjelutung (Masa Pemerintahan ± 35 Musim) | Era Kerajaan Tidung - Sejarah
Yamus (Si Amus) (Masa Pemerintahan ± 44 Musim) | Era Kerajaan Tidung - Sejarah
Ibugang (Aki Bugang) | Era Kerajaan Tidung - Sejarah
Itara (Lebih kurang 29 Musim) | Era Kerajaan Tidung - Sejarah
Ikurung (Lebih kurang 25 Musim) | Era Kerajaan Tidung - Sejarah
Ikarang (Lebih kurang 35 Musim), di Tanjung Batu (Tarakan). | Era Kerajaan Tidung - Sejarah
Karangan (Lebih kurang Musim) | Era Kerajaan Tidung - Sejarah
Ibidang (Lebih kurang Musim) | Era Kerajaan Tidung - Sejarah
Bengawan (Lebih kurang 44 Musim) | Era Kerajaan Tidung - Sejarah
Itambu (Lebih kurang 20 Musim) | Era Kerajaan Tidung - Sejarah
Aji Beruwing Sakti (Lebih kurang 30 Musim) | Era Kerajaan Tidung - Sejarah
Aji Surya Sakti (Lebih kurang 30 Musim) | Era Kerajaan Tidung - Sejarah
Aji Pengiran Kungun (Lebih kurang 25 Musim) | Era Kerajaan Tidung - Sejarah
Pengiran Tempuad (Lebih kurang 34 Musim) | Era Kerajaan Tidung - Sejarah
Aji Iram Sakti (Lebih kurang 25 Musim) di Pimping, Bulungan | Era Kerajaan Tidung - Sejarah
Aji Baran Sakti (Lebih kurang 20 Musim). | Era Kerajaan Tidung - Sejarah
Datoe Mancang (Lebih kurang 49 Musim) | Era Kerajaan Tidung - Sejarah
Abang Lemanak (Lebih kurang 20 Musim), di Baratan, Bulungan | Era Kerajaan Tidung - Sejarah
Ikenawai bergelar Ratu Ulam Sari (Lebih kurang 15 Musim) | Era Kerajaan Tidung - Sejarah
