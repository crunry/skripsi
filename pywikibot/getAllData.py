#!/usr/bin/python
# -- coding: utf-8 --

from __future__ import print_function
from SPARQLWrapper import SPARQLWrapper, XML
import xml.etree.ElementTree as ET
# import goslate
import codecs
import sys
from googletrans import Translator
from list_function import make_array

reload(sys)  
sys.setdefaultencoding('utf8')

# gs = goslate.Goslate()
translator = Translator()

array_all_heritage_wikidata = make_array()
array_all_heritage_dbpedia = make_array()
array_all_heritage_dbpedia_id = make_array()

array_all_heritage_wikidata_uri = make_array()
array_all_heritage_dbpedia_uri = make_array()
array_all_heritage_dbpedia_id_uri = make_array()
sparql_wikidata = SPARQLWrapper("https://query.wikidata.org/sparql")
sparql_dbpedia = SPARQLWrapper("http://dbpedia.org/sparql")
sparql_dbpedia_id = SPARQLWrapper("http://id.dbpedia.org/sparql")

translate_error = open("translate_error.txt", "w")

with open("query_wikidata.txt", 'r') as allquery:
	for linequery in allquery:
		partscoll = linequery.split(",", 1)
		kategori = int(partscoll[0].strip())
		query = partscoll[1].strip()
		sparql_wikidata.setQuery(query)
		sparql_wikidata.setReturnFormat(XML)
		results = sparql_wikidata.query().convert()
		root = ET.fromstring(results.toxml().encode('utf-8'))
		for literal in root.iter("{http://www.w3.org/2005/sparql-results#}literal"):
			array_all_heritage_wikidata[kategori].append(literal.text.encode('utf-8'))
		for uri in root.iter("{http://www.w3.org/2005/sparql-results#}uri"):
			array_all_heritage_wikidata_uri[kategori].append(uri.text.encode('utf-8'))

print ("pengumpulan data dari wikidata done...")

with open("query_dbpedia.txt", 'r') as allquery:
	for linequery in allquery:
		partscoll = linequery.split(",", 1)
		kategori = int(partscoll[0].strip())
		query = partscoll[1].strip()
		sparql_dbpedia.setQuery(query)
		sparql_dbpedia.setReturnFormat(XML)
		results = sparql_dbpedia.query().convert()
		root = ET.fromstring(results.toxml().encode('utf-8'))
		for literal in root.iter("{http://www.w3.org/2005/sparql-results#}literal"):
			try:
				translated = translator.translate(literal.text.encode('utf-8'), src='en', dest='id').text
				# array_all_heritage_dbpedia_id[kategori].append(gs.translate(literal.text.encode('utf-8'), 'en', 'id'))
				array_all_heritage_dbpedia[kategori].append(translated)
			except:
				array_all_heritage_dbpedia[kategori].append("error")
				print (literal.text.encode('utf-8'), file=translate_error)
		for uri in root.iter("{http://www.w3.org/2005/sparql-results#}uri"):
			array_all_heritage_dbpedia_uri[kategori].append(uri.text.encode('utf-8'))

print ("pengumpulan data dari dbpedia internasional done...")

with open("query_dbpedia_ind.txt", 'r') as allquery:
	for linequery in allquery:
		partscoll = linequery.split(",", 1)
		kategori = int(partscoll[0].strip())
		query = partscoll[1].strip()
		sparql_dbpedia_id.setQuery(query)
		sparql_dbpedia_id.setReturnFormat(XML)
		results = sparql_dbpedia_id.query().convert()
		root = ET.fromstring(results.toxml().encode('utf-8'))
		for literal in root.iter("{http://www.w3.org/2005/sparql-results#}literal"):
			array_all_heritage_dbpedia_id[kategori].append(literal.text.encode('utf-8'))
		for uri in root.iter("{http://www.w3.org/2005/sparql-results#}uri"):
			array_all_heritage_dbpedia_id_uri[kategori].append(uri.text.encode('utf-8'))

print ("pengumpulan data dari dbpedia indonesia done...")

print ("proses printing...")
f_collect = codecs.open('hasil_pengumpulan_data.xls', 'w')
#menuliskan data wikidata
index_kategori = 0
for arr_category1, arr_category2 in zip(array_all_heritage_wikidata, array_all_heritage_wikidata_uri):
	for literal, uri in zip(arr_category1, arr_category2):
		f_collect.write(str(index_kategori) + "\t" + literal + "\t" + uri + "\n")
	index_kategori = index_kategori + 1
#menuliskan data dbpedia internasional
index_kategori = 0
for arr_category1, arr_category2 in zip(array_all_heritage_dbpedia, array_all_heritage_dbpedia_uri):
	for literal, uri in zip(arr_category1, arr_category2):
		f_collect.write(str(index_kategori) + "\t" + literal + "\t" + uri + "\n")
	index_kategori = index_kategori + 1
#menuliskan data dbpedia internasional
index_kategori = 0
for arr_category1, arr_category2 in zip(array_all_heritage_dbpedia_id, array_all_heritage_dbpedia_id_uri):
	for literal, uri in zip(arr_category1, arr_category2):
		f_collect.write(str(index_kategori) + "\t" + literal + "\t" + uri + "\n")
	index_kategori = index_kategori + 1
f_collect.close()
print ("printing done...")