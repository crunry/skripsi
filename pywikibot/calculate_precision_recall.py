#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import print_function
import codecs
from list_function import make_array, make_empty

f_differ = codecs.open("differ.csv", "w")

def intersection(lst1, lst2):
    lst3 = [value for value in lst1 if value in lst2]
    return lst3

def diff(first, second):
    second = set(second)
    return [item for item in first if item not in second]

arr_state_gold = []
#menghitung precision recall
with open('gold.csv', 'r') as gold:
	for line in gold:
		parts = line.split(",")
		provinsi = parts[0].lower().strip()
		kategori = parts[1].lower().strip()
		entity = parts[2].lower().strip()
		arr_state_gold.append(provinsi + "|" + kategori + "|" + entity)

arr_state_prov = []
with open('hasil_ekstraksi_provinsi.csv', 'r') as prov:
	for line in prov:
		parts = line.split(",")
		provinsi = parts[0].lower().strip()
		kategori = parts[1].lower().strip()
		entity = parts[2].lower().strip()
		arr_state_prov.append(provinsi + "|" + kategori + "|" + entity)

intersect_gold_prov = intersection(arr_state_gold, arr_state_prov)
print (len(intersect_gold_prov))
print (len(arr_state_prov))
differ_prov_intersect = diff(arr_state_prov, intersect_gold_prov)
for elem in differ_prov_intersect:
	f_differ.write(elem.replace("|", "\t") + "\n")
print (len(arr_state_gold))
f_differ.close()





		
