pembuatan ontologi E-CHNH berdasarkan metodologi Uschold and King
metodologi perancangan ontologi secara garis besar meliputi tiga tahap pengembangan: identifikasi tujuan, pembangunan ontologi, dan evaluasi

identifikasi tujuan dibagi 2: menentukan tujuan dan menentukan ruang lingkup ontologi E-CHNH. 
pada tahap kedua, membangun ontologi, dibagi menjadi tiga sub-tahap, yaitu ontology capture, ontology coding, dan integrasi dengan ontologi yang sudah ada

dikumpulkan konsep2 dan hubungan antar konsep menggunakan pendekatan kualitatif berdasarkan hasil diskusi, tinjauan literatur, dan hasil riset kerangka e-CHNH. 

domain ontologi dikodekan dalam representasi semi-formal menggunakan DL. abis dibuat semiformal dibuat jadi formal dalam OWL agar dapat diproses secara otomatis oleh komputer

tahap ketiga: evaluasi utk memastikan konsistensi semantik dari ontologi yg telah dibuat. 

sumber utama istilah dan konsep yang dijadikan sebagai class dan property pada ontologi e-chnh adalah basis data e-chnh yang merupakan hasil riset sebelumnya oleh Pak Zainal dan timnya. Utk itu perlu ada pemetaan yg tepat dari basis data relasional yang digunakan sebagai repository chnh menjadi bentuk ontologi. digunakan metode db2owl utk melakukan pemetaan basis data ke dalam ontologi.

pemetaan: proses di mana suatu basis data dan suatu ontologi terhubung secara semantik pada level konseptual. pemetaan ada dua proses: pemetaan definisi, yaitu proses transformasi skema basis data menjadi struktur ontologi dan 2) pemindahan atau migrasi data, yaitu pemindahan isi basis data menjadi instance pada ontologi. perpindahan itu disebut juga populasi ontologi dan dapat dilakukan dengan dua cara: mengumpulkan semua instance pada basis data ke dalam ontologi, atau hanya mentransformasikan instance basis data yang akan menjadi respon (jawaban) bagi query tertentu saja.

proses pemetaan dimulai dari mendeteksi beberapa kasus pada tabel skema basis data. utk kasus yang terjadi, tiap komponen basis data yaitu tabel, kolom, dan constraint dikonversi menjadi komponen ontologi yang sesuai (class, property, dan relasi). 

yang masuk ke dalam kasus 3:
jenis warisan budaya, lokasi, object 

setelah masuk ke dalam kasus2:
cultural heritage object menjadi subclass object
immovable cultural heritage subclass dari tangible cultural heritage
tangible cultural heritage dan intangible cultural heritage subclass dari cultural heritage object


perayaan adalah cultural work
eksibisi adalah cultural work
ritual adalah cultural work
tradisi lisan adalah cultural work
object has_location_origin location

angklung isindividualof alat musik tradisional
bali isindividualof location

yang termasuk ke kasus tabel 3
lokasi fisik objek warisan budaya
lokasi asal warisan budaya tak benda
jenis warisan budaya
lokasi
jenis objek
objek

yang termasuk ke kasus tabel 2
objek warisan budaya
objek warisan budaya tak bergerak
objek warisan budaya tak benda
objek budaya bergerak

yang dimaksud karya budaya itu yg man made object

nama predikatnya has_current_location

==HASIL DAN PEMBAHASAN==



