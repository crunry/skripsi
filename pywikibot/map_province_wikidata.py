from __future__ import print_function
import requests
import re

f_mapping_province = open("province_mapping.txt", "w")

print ("PROGRAM STARTS")
with open('province.txt', 'r') as provs:
	for line in provs:
		entitas = line.strip()
		api_id = "https://www.wikidata.org/w/api.php?action=wbsearchentities&search=" + entitas + "&language=id&format=json"
		response_id = requests.get(api_id)
		data_id = response_id.json()
		#mendapatkan id Q
		search_result = data_id["search"]
		for index in range(len(search_result)):
			try:
				description = search_result[index]["description"]
				preceded_province = re.match(r'^province\s(.)+', description, re.I)
				if preceded_province:
					print (entitas + "\t" + search_result[index]["title"], file=f_mapping_province)
			except:
				pass
print ("MAPPING IS DONE")